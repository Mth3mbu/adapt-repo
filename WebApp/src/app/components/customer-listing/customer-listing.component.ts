import { LiveAnnouncer } from '@angular/cdk/a11y';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/interfaces/appState';
import { CustomerDto } from 'src/app/interfaces/customerDto';
import { CustomerService } from 'src/app/services/customer.service';
import * as CustomerActions from '../../store/actions/customer.action';

@Component({
  selector: 'app-customer-listing',
  templateUrl: './customer-listing.component.html',
  styleUrls: ['./customer-listing.component.scss']
})
export class CustomerListingComponent implements OnInit, AfterViewInit {

  displayedColumns = ['name', 'surname', 'email', 'phone', 'total', 'action'];
  dataSource = new MatTableDataSource<CustomerDto>([]);

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private _liveAnnouncer: LiveAnnouncer,
    private router: Router,
    private store: Store<AppState>,
    private customerService: CustomerService) { }

  ngOnInit(): void {
    this.store.dispatch(new CustomerActions.GetCustomersStart());
    this.store.select('customer').subscribe(customerStateSlice => {
      if (this.paginator) {
        this.paginator.length = customerStateSlice.customers.length;
      }
      this.dataSource = new MatTableDataSource<CustomerDto>(customerStateSlice.customers)
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onCreateCustomerClick() {
    this.store.dispatch(new CustomerActions.SetEditMode(false));
    this.router.navigate(['/new'])
  }

  onViewClick(customer: CustomerDto) {
    this.store.dispatch(new CustomerActions.SetCustomer(customer));
    this.store.dispatch(new CustomerActions.SetEditMode(true));
    this.router.navigate(['/update'])
  }

  onDeleteClick(customerId: string) {
    this.customerService.deleteCustomer(customerId).subscribe(res => {
      this.store.dispatch(new CustomerActions.GetCustomersStart());
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      return;
    }

    const sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name':
          return compare(a.firstName.toLowerCase(), b.firstName.toLowerCase(), isAsc);
        case 'surname':
          return compare(a.lastName.toLowerCase(), b.lastName.toLowerCase(), isAsc);
        case 'email':
          return compare(a.email.toLowerCase(), b.email.toLowerCase(), isAsc);
        case 'phone':
          return compare(a.phone.toLowerCase(), b.phone.toLowerCase(), isAsc);
        case 'total':
          return compare(a.balance, b.balance, isAsc);
        default:
          return 0;
      }
    });

    this.dataSource.data = sortedData;
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
