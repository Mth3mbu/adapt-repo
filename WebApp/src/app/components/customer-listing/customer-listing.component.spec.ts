import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { CustomerDto } from 'src/app/interfaces/customerDto';
import { CustomerService } from 'src/app/services/customer.service';
import { CustomerServiceTestDataBuilder } from 'src/app/utest-tils/customer.service.test.data.builder';
import { CustomerDtoDataBuilder } from 'src/app/utest-tils/customerDto.testDataBuolder';
import { TestFixtureFactory } from 'src/app/utest-tils/testFixtureFactory';
import { CustomerListingComponent } from './customer-listing.component';

describe('CustomerListingComponent', () => {
  describe('Customer Listing', () => {
    it('Should display customers to the screen', async () => {
      // Arrange
      const service = createFakeCustomerServiceService();
      const fixture = await createFixture(service);
      // Act
      const component = fixture.componentFixture.componentInstance;
      // Assert
      expect(component).toBeTruthy();
      expect(component.dataSource.data.length).toBe(10);
    })

    it('Should display create new customer button', async () => {
      // Arrange
      const service = createFakeCustomerServiceService();
      const fixture = await createFixture(service);
      // Act
      const button = fixture.componentFixture.debugElement.query(By.css('.create-customer-btn')).nativeElement;
      // Assert
      expect(button).toBeDefined();
      expect(button.innerHTML).toBe('CREATE NEW CUSTOMER');
    })

    it('Should navigate to create new customer screen', async () => {
      // Arrange
      const service = createFakeCustomerServiceService();
      const fixture = await createFixture(service);
      const component = fixture.componentFixture.componentInstance;
      // Act
      component.onCreateCustomerClick();
      // Assert
      expect(fixture.router.navigate).toHaveBeenCalledWith(['/new']);
    })

    it('Should have search box', async () => {
      // Arrange
      const service = createFakeCustomerServiceService();
      const fixture = await createFixture(service);
      // Act
      const searchBox = fixture.componentFixture.nativeElement.querySelectorAll('input');
      // Assert
      expect(searchBox).toBeTruthy();
    })

    it('Should display view customer options', async () => {
      // Arrange
      const customers = CustomerDtoDataBuilder
        .create().buildList(10);
      const service = createFakeCustomerServiceService(customers);
      const fixture = await createFixture(service);
      // Act
      const viewButton = await fixture.componentFixture.nativeElement.querySelectorAll('.view-btn');
      // Assert
      expect(viewButton).toBeDefined();
      expect(viewButton.length).toBe(5);
    })

    it('Should display delete customer buttons', async () => {
      // Arrange
      const customers = CustomerDtoDataBuilder
        .create().buildList(10);
      const service = createFakeCustomerServiceService(customers);
      const fixture = await createFixture(service);
      // Act
      const deleteButtons = await fixture.componentFixture.nativeElement.querySelectorAll('.delete-btn');
      // Assert
      expect(deleteButtons).toBeDefined();
      expect(deleteButtons.length).toBe(5);
    });
  })
});


function createFakeCustomerServiceService(customers?: CustomerDto[]) {
  const fakeCustomerService = jasmine.createSpyObj<CustomerService>(
    [
      'getCustomers'
    ]
  );

  customers = customers || CustomerDtoDataBuilder
    .create().buildList(10);
  fakeCustomerService.getCustomers.and.returnValue(of(customers));

  return fakeCustomerService;
}

function createFixture(customerService?: CustomerService) {
  return TestFixtureFactory.createForComponent(CustomerListingComponent,
    (testBed) => testBed.overrideProvider(
      CustomerService,
      { useValue: customerService || CustomerServiceTestDataBuilder.create().withRandomProps().withGetAllCustomers().build() }
    )
  );
}
