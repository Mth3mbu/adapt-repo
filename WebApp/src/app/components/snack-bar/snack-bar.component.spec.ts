import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TestFixtureFactory } from 'src/app/utest-tils/testFixtureFactory';
import { SnackBarComponent } from './snack-bar.component';

xdescribe('SnackBarComponent', () => {
  it('should create', async () => {
    const fixture = await createFixture();
    const component = await fixture.componentFixture.componentInstance;
    expect(component).toBeTruthy();
  });
});

function createFixture() {
  return TestFixtureFactory.createForComponent(SnackBarComponent,
    (testBed) => testBed.overrideProvider(
      MAT_DIALOG_DATA,
      { useValue: {} }
    ).overrideProvider(MatDialogRef, {
      useValue: {}
    })
  );
}
