import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Guid } from 'guid-typescript';
import { Customer } from 'src/app/interfaces/customer';
import { CustomerType } from 'src/app/interfaces/customerType';
import { CustomerTypeService } from 'src/app/services/customer-type.service';
import { CustomerService } from 'src/app/services/customer.service';
import { Router } from "@angular/router"
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/interfaces/appState';
import { CustomerDto } from 'src/app/interfaces/customerDto';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  formGroup: any;
  titleAlert: string = 'This field is required';
  post: any = '';
  customerTypes!: CustomerType[];
  customer?: CustomerDto;
  isEditMode: boolean = false;

  constructor(private formBuilder: FormBuilder,
    private customerTypeService: CustomerTypeService,
    private customerService: CustomerService,
    private router: Router,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.store.select('customer').subscribe(customerStateSlice => {
      this.customer = customerStateSlice.customer;
      this.isEditMode = customerStateSlice.isEditMode
    });

    this.createForm();
    this.customerTypeService.getCustomerTypes().subscribe(response => {
      this.customerTypes = response;
    });
  }

  onSUbmit() {
    const customerId = this.customer?.id ?? Guid.create().toString();
    const customer: Customer = {
      lastName: this.formGroup.value.surname,
      firstName: this.formGroup.value.name,
      id: this.isEditMode ? customerId : Guid.create().toString(),
      typeId: this.formGroup.value.type,
      email: this.formGroup.value.email,
      phone: this.formGroup.value.phone
    }
    const balance = parseInt(this.formGroup.value.total);
    if (!this.isEditMode) {
      this.customerService.createCustomer(customer, balance).subscribe(res => {
        this.navigateHome();
      });
    } else {
      this.customerService.updateCustomer(customer, balance).subscribe(res => {
        this.navigateHome()
      });
    }
  }

  navigateHome() {
    this.router.navigate(['/home'])
  }
  createForm() {
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    this.formGroup = this.formBuilder.group({
      'email': [this.isEditMode ? this.customer?.email : '', [Validators.required, Validators.pattern(emailregex)]],
      'name': [this.isEditMode ? this.customer?.firstName : '', Validators.required],
      'surname': [this.isEditMode ? this.customer?.lastName : '', [Validators.required]],
      'phone': [this.isEditMode ? this.customer?.phone : '', [Validators.required]],
      'type': [this.isEditMode ? this.customer?.typeId : '', [Validators.required]],
      'total': [this.isEditMode ? this.customer?.balance : '', [Validators.required]],
      'validate': ''
    });
  }

  get name() {
    return this.formGroup.get('name') as FormControl
  }

  getError(propName: string) {
    return this.formGroup.get(propName).hasError('required') ? 'Field is required' :
      this.formGroup.get(propName).hasError('pattern') ? `Invalid ${propName}` : this.formGroup.get(propName).hasError('minlength') ? 'Invalid Cellphone number'
        : this.formGroup.get(propName).hasError('maxlength') ? 'Invalid Cellphone number' : ''
  }

  onSubmit(post: any) {
    this.post = post;
  }

}
