import { CustomerComponent } from './customer.component';
import { CustomerTypeService } from 'src/app/services/customer-type.service';
import { CustomerService } from 'src/app/services/customer.service';
import { CustomerServiceTestDataBuilder } from 'src/app/utest-tils/customer.service.test.data.builder';
import { of } from 'rxjs';
import { CustomerDto } from 'src/app/interfaces/customerDto';
import { CustomerDtoDataBuilder } from 'src/app/utest-tils/customerDto.testDataBuolder';
import { TestFixtureFactory } from 'src/app/utest-tils/testFixtureFactory';
import { CustomerTypeTestDataBuilder } from 'src/app/utest-tils/customer.type.testDataBuilder';
import { MatSelectHarness } from '@angular/material/select/testing';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { CustomerType } from 'src/app/interfaces/customerType';
import { CostomerTypeServiceTestDataBuilder } from 'src/app/utest-tils/customer.ype.service.testDataBuilder';

describe('CustomerComponent', () => {
  it('should have customer types drop down list', async () => {
    // Arrange
    const customerTypes = CustomerTypeTestDataBuilder
      .create().buildList(3);
    const customerService = createFakeCustomerServiceService();
    const fixture = await createFixture(customerService, undefined, customerTypes);
    // Act
    const dropDown = await fixture.loader.getHarness(MatSelectHarness);
    await dropDown.open();
    // Assert
    expect(dropDown).toBeDefined();
    expect((await dropDown.getOptions()).length).toBe(3);
  });

  it('should have email text input', async () => {
    // Arrange
    const customerTypes = CustomerTypeTestDataBuilder
      .create().buildList(3);
    const customerService = createFakeCustomerServiceService();
    const fixture = await createFixture(customerService, undefined, customerTypes);
    // Act
    const emailTextInput = await fixture.loader.getHarness(MatInputHarness.with({ 'placeholder': 'Email' }));
    // Assert
    expect(emailTextInput).toBeDefined();
  });


  it('should have first name text input', async () => {
    // Arrange
    const customerTypes = CustomerTypeTestDataBuilder
      .create().buildList(3);
    const customerService = createFakeCustomerServiceService();
    const fixture = await createFixture(customerService, undefined, customerTypes);
    // Act
    const emailTextInput = await fixture.loader.getHarness(MatInputHarness.with({ 'placeholder': 'First Name' }));
    // Assert
    expect(emailTextInput).toBeDefined();
  });

  it('should have last name text input', async () => {
    // Arrange
    const customerTypes = CustomerTypeTestDataBuilder
      .create().buildList(3);
    const customerService = createFakeCustomerServiceService();
    const fixture = await createFixture(customerService, undefined, customerTypes);
    // Act
    const emailTextInput = await fixture.loader.getHarness(MatInputHarness.with({ 'placeholder': 'Last Name' }));
    // Assert
    expect(emailTextInput).toBeDefined();
  });


  it('should have phone text input', async () => {
    // Arrange
    const customerTypes = CustomerTypeTestDataBuilder
      .create().buildList(3);
    const customerService = createFakeCustomerServiceService();
    const fixture = await createFixture(customerService, undefined, customerTypes);
    // Act
    const emailTextInput = await fixture.loader.getHarness(MatInputHarness.with({ 'placeholder': 'Cellphone' }));
    // Assert
    expect(emailTextInput).toBeDefined();
  });


  it('should have invoice total text input', async () => {
    // Arrange
    const customerTypes = CustomerTypeTestDataBuilder
      .create().buildList(3);
    const customerService = createFakeCustomerServiceService();
    const fixture = await createFixture(customerService, undefined, customerTypes);
    // Act
    const emailTextInput = await fixture.loader.getHarness(MatInputHarness.with({ 'placeholder': 'Invoce Total' }));
    // Assert
    expect(emailTextInput).toBeDefined();
  });

  it('should have save button', async () => {
    // Arrange
    const customerTypes = CustomerTypeTestDataBuilder
      .create().buildList(3);
    const customerService = createFakeCustomerServiceService();
    const fixture = await createFixture(customerService, undefined, customerTypes);
    // Act
    const saveButton = await fixture.loader.getHarness(MatButtonHarness.with({ text: 'SAVE' }));
    // Assert
    expect(saveButton).toBeDefined();
  });

  it('should have back button', async () => {
    // Arrange
    const customerTypes = CustomerTypeTestDataBuilder
      .create().buildList(3);
    const customerService = createFakeCustomerServiceService();
    const fixture = await createFixture(customerService, undefined, customerTypes);
    // Act
    const saveButton = await fixture.loader.getHarness(MatButtonHarness.with({ text: 'BACK TO LIST' }));
    // Assert
    expect(saveButton).toBeDefined();
  });

  it('should show required errors', async () => {
    // Arrange
    const customerTypes = CustomerTypeTestDataBuilder
      .create().buildList(3);
    const customerService = createFakeCustomerServiceService();
    const fixture = await createFixture(customerService, undefined, customerTypes);
    // Act
    const saveButton = await fixture.loader.getHarness(MatButtonHarness.with({ text: 'SAVE' }));
    await saveButton.click();
    // Assert
    const erros = await fixture.componentFixture.nativeElement.querySelectorAll('mat-error');
    expect(erros).toBeDefined();
  });
});



function createFakeCustomerServiceService(customers?: CustomerDto[]) {
  const fakeCustomerService = jasmine.createSpyObj<CustomerService>(
    [
      'getCustomers'
    ]
  );

  customers = customers || CustomerDtoDataBuilder
    .create().buildList(10);
  fakeCustomerService.getCustomers.and.returnValue(of(customers));

  return fakeCustomerService;
}

function createFixture(customerService?: CustomerService, customerTypeService?: CustomerTypeService, customerTypes?: CustomerType[]) {
  return TestFixtureFactory.createForComponent(CustomerComponent,
    (testBed) => testBed.overrideProvider(
      CustomerService,
      {
        useValue: customerService || CustomerServiceTestDataBuilder.create()
          .withRandomProps()
          .build()
      }
    ).overrideProvider(
      CustomerTypeService,
      {
        useValue: customerTypeService || CostomerTypeServiceTestDataBuilder.create()
          .withRandomProps()
          .withGetAllCustomerTypes(customerTypes).build()
      }
    )
  );
}
