import { CustomerDto } from './customerDto';

export interface CustomerState {
  customers: CustomerDto[],
  customer?: CustomerDto,
  isEditMode: boolean,
  error: ''
}
