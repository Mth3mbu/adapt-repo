import { CustomerState } from './customer-state';

export interface AppState {
  customer: CustomerState;
}
