export interface CustomerDto {
  id: string,
  typeId: string,
  firstName: string;
  lastName: string;
  email: string,
  phone: string,
  balance: number
}
