export interface Customer{
  id: string,
  typeId: string,
  firstName: string;
  lastName:string;
  email:string,
  phone: string
}
