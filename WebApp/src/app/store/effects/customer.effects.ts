import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { switchMap, map, catchError, of } from 'rxjs';
import { CustomerService } from 'src/app/services/customer.service';
import * as CustomerActions from '../actions/customer.action';

@Injectable()
export class CustomerEffects {
  @Effect() getCustomers = this.actions$
    .pipe(ofType(CustomerActions.GET_CUSTOMERS_START),
      switchMap(
        () => this.customerService.getCustomers()
          .pipe(
            map(res => new CustomerActions.GetCustomers(res)
            ),
            catchError(error => of(new CustomerActions.GetTotalProducsFail(error?.error?.message)))
          )
      )
    );

  constructor(private actions$: Actions, private customerService: CustomerService) { }
}
