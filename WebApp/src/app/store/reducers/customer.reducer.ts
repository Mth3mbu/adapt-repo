import { CustomerState } from 'src/app/interfaces/customer-state';
import * as CustomerActions from '../actions/customer.action';


const initialState: CustomerState = {
  customers: [],
  customer: undefined,
  isEditMode: false,
  error: ''
}

export function customerReducer(state = initialState, action: any) {
  switch (action.type) {
    case CustomerActions.GET_CUSTOMERS:
      return { ...state, customers: action.payload }

    case CustomerActions.GET_CUSTOMERS_FAIL:
      return { ...state, error: action.payload }

    case CustomerActions.GET_CUSTOMERS_START:
      return { ...state }

    case CustomerActions.SET_CUSTOMER:
      return { ...state, customer: action.payload }

    case CustomerActions.SET_EDIT_MODE:
      return { ...state, isEditMode: action.payload }

    default: {
      return state;
    };
  }
}
