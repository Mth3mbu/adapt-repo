import { Action } from '@ngrx/store';
import { CustomerDto } from 'src/app/interfaces/customerDto';


export const GET_CUSTOMERS_START = "[Customer] customers start";
export const GET_CUSTOMERS = "[Customer] customers";
export const GET_CUSTOMERS_FAIL = "[Customer] Get customers fail"
export const SET_CUSTOMER = "[Customer] Set Customer"
export const SET_EDIT_MODE = "[Customer] Set Edit Mode"

export class GetCustomersStart implements Action {
  readonly type = GET_CUSTOMERS_START;
}

export class GetCustomers implements Action {
  readonly type = GET_CUSTOMERS;
  constructor(public payload: CustomerDto[]) { }
}

export class GetTotalProducsFail implements Action {
  readonly type = GET_CUSTOMERS_FAIL;
  constructor(public payload: string) { }
}

export class SetCustomer implements Action {
  readonly type = SET_CUSTOMER;
  constructor(public payload: CustomerDto) { }
}

export class SetEditMode implements Action {
  readonly type = SET_EDIT_MODE;
  constructor(public payload: boolean) { }
}
