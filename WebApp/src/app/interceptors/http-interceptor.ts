import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../components/snack-bar/snack-bar.component';


@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  durationInSeconds = 5;
  defaultErrorMessage = 'Something went wrong while attempting to connect to the server, Please check you internet connection';
  constructor(private snackBar: MatSnackBar) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(error => {
        const message = this.getErrorMessage(error);
        this.snackBar.openFromComponent(
          SnackBarComponent,
          {
            data: message,
            duration: 10000,
            panelClass: 'error-snackbar'
          });
        return throwError(error.message);
      })
    );
  }

  getErrorMessage(error: any) {
    const isValidationError =error.error.message.includes('Already');
    return isValidationError ? error.error.message: this.defaultErrorMessage;
  }
}
