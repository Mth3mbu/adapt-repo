import { TestBed, ComponentFixture, TestBedStatic } from '@angular/core/testing';
import { AppModule } from '../app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { Type, InjectionToken, AbstractType } from '@angular/core';
import { HarnessLoader } from '@angular/cdk/testing';
import { Router } from '@angular/router';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';

export class TestFixtureFactory {
    public static async createForComponent<T>(
        componentType: Type<T>,
        testBedCustomizer?: (testBed: TestBedStatic) => void): Promise<ComponentTestFixture<T>> {
        const testBed = TestBed.configureTestingModule({
            imports: [
                AppModule,
                MatInputModule,
                MatSelectModule,
                MatRadioModule,
                MatCardModule,
                RouterTestingModule,
                HttpClientTestingModule
            ],
            declarations: [componentType],
        });

        if (testBedCustomizer) {
            testBedCustomizer(testBed);
        }

        await testBed.compileComponents();

        const componentFixture = TestBed.createComponent<T>(componentType);
        const loader = TestbedHarnessEnvironment.loader(componentFixture);

        const router = testBed.inject(Router);
        spyOn(router, 'navigate');

        const httpMock = testBed.inject(HttpTestingController);

        componentFixture.detectChanges();

        return {
            loader,
            componentFixture,
            router,
            httpMock
        };
    }

    public static async createForService<T>(
        serviceType: Type<T> | InjectionToken<T> | AbstractType<T>,
        testBedCustomizer?: (testBed: TestBedStatic) => void): Promise<ServiceTestFixture<T>> {

        const testBed = TestBed.configureTestingModule({
            imports: [
                AppModule,

                RouterTestingModule,
                HttpClientTestingModule
            ]
        });

        if (testBedCustomizer) {
            testBedCustomizer(testBed);
        }

        const router = testBed.inject(Router);
        spyOn(router, 'navigate');

        const httpMock = testBed.inject(HttpTestingController);

        const service = testBed.inject(serviceType);

        return {
            service,
            router,
            httpMock
        };
    }

    public static async createForServiceWithAuth<T>(
        serviceType: Type<T> | InjectionToken<T> | AbstractType<T>,
        testBedCustomizer?: (testBed: TestBedStatic) => void): Promise<ServiceTestFixture<T>> {
        const testBed = TestBed.configureTestingModule({
            imports: [
                AppModule,
                RouterTestingModule,
                HttpClientTestingModule
            ]
        });
        if (testBedCustomizer) {
            testBedCustomizer(testBed);
        }

        const router = testBed.inject(Router);
        spyOn(router, 'navigate');

        const httpMock = testBed.inject(HttpTestingController);

        const service = testBed.inject(serviceType);

        return {
            service,
            router,
            httpMock
        };
    }
}

export interface ComponentTestFixture<T> {
    loader: HarnessLoader;
    componentFixture: ComponentFixture<T>;
    router: Router;
    httpMock: HttpTestingController;
}

export interface ServiceTestFixture<T> {
    service: T;
    router: Router;
    httpMock: HttpTestingController;
}
