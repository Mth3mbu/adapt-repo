import * as faker from 'faker/locale/en';
import { of } from 'rxjs';
import { Customer } from '../interfaces/customer';
import { CustomerDto } from '../interfaces/customerDto';
import { CustomerService } from '../services/customer.service';
import { CustomerTestDataBuilder } from './customer.testDataBuilder';
import { CustomerDtoDataBuilder } from './customerDto.testDataBuolder';
import { TestDataBuilder } from './test.data.builder';

export class CustomerServiceTestDataBuilder extends TestDataBuilder<CustomerServiceTestDataBuilder, jasmine.SpyObj<CustomerService>>{
  constructor() {
    super(() => {
      return jasmine.createSpyObj<CustomerService>(['getCustomers', 'createCustomer', 'updateCustomer', 'deleteCustomer']);
    });
  }

  static create() {
    return new CustomerServiceTestDataBuilder();
  }

  withGetAllCustomers(customers?: CustomerDto[]) {
    const expectedValue = customers || CustomerDtoDataBuilder.create().buildList(10);
    return this.withProp(o => o.getCustomers.and.returnValue(of(expectedValue)))
  }

  withCreateCustomer(customer?: Customer) {
    const expectedValue = customer || CustomerTestDataBuilder.create().withRandomProps();
    return this.withProp(o => o.createCustomer.and.returnValue(of(expectedValue)))
  }

  withUpdate(customer: Customer) {
    const expectedValue = customer || CustomerTestDataBuilder.create().withRandomProps();
    return this.withProp(o => o.updateCustomer.and.returnValue(of(expectedValue)))
  }

  withDelete(customerId: string) {
    const expectedValue = customerId || faker.random.word();
    return this.withProp(o => o.deleteCustomer.and.returnValue(of(expectedValue)))
  }

  withRandomProps() {
    return this;
  }
}
