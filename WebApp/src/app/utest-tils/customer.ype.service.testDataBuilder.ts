import { of } from 'rxjs';
import { CustomerType } from '../interfaces/customerType';
import { CustomerTypeService } from '../services/customer-type.service';
import { CustomerTypeTestDataBuilder } from './customer.type.testDataBuilder';
import { TestDataBuilder } from './test.data.builder';

export class CostomerTypeServiceTestDataBuilder extends TestDataBuilder<CostomerTypeServiceTestDataBuilder, jasmine.SpyObj<CustomerTypeService>>{
  constructor() {
    super(() => {
      return jasmine.createSpyObj<CustomerTypeService>(['getCustomerTypes']);
    });
  }

  static create() {
    return new CostomerTypeServiceTestDataBuilder();
  }

  withGetAllCustomerTypes(customerTypes?: CustomerType[]) {
    const expectedValue = customerTypes || CustomerTypeTestDataBuilder.create().buildList(5);
    return this.withProp(o => o.getCustomerTypes.and.returnValue(of(expectedValue)))
  }

  withRandomProps() {
    return this;
  }
}
