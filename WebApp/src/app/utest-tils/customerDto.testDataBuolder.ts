import { CustomerDto } from '../interfaces/customerDto';
import { TestDataBuilder } from './test.data.builder';
import * as faker from 'faker';

export class CustomerDtoDataBuilder extends TestDataBuilder<CustomerDtoDataBuilder, CustomerDto>{
  constructor() {
    super(() => {
      return {} as CustomerDto;
    });
  }
  static create() {
    return new CustomerDtoDataBuilder();
  }
  withId(id: string) {
    return this.withProp(o => o.id = id);
  }

  withTypeId(value: string) {
    return this.withProp(o => o.typeId = value);
  }

  withFirstName(value: string) {
    return this.withProp(o => o.firstName = value);
  }

  withLastName(value: string) {
    return this.withProp(o => o.lastName = value);
  }

  withEmail(value: string) {
    return this.withProp(o => o.email = value);
  }

  withPhone(value: string) {
    return this.withProp(o => o.phone = value);
  }

  withBalance(value: number) {
    return this.withProp(o => o.balance = value);
  }

  withRandomProps() {
    return this.withId(faker.random.alphaNumeric())
      .withTypeId(faker.name.findName())
      .withLastName(faker.name.lastName())
      .withEmail(faker.random.word())
      .withPhone(faker.phone.phoneNumber())
      .withBalance(faker.random.number())
  }
}
