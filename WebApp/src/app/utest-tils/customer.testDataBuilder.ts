import * as faker from 'faker';
import { Customer } from '../interfaces/customer';
import { TestDataBuilder } from './test.data.builder'

export class CustomerTestDataBuilder extends TestDataBuilder<CustomerTestDataBuilder, Customer> {
  constructor() {
    super(() => {
      return {} as Customer;
    });
  }
  static create() {
    return new CustomerTestDataBuilder();
  }
  withId(id: string) {
    return this.withProp(o => o.id = id);
  }

  withTypeId(value: string) {
    return this.withProp(o => o.typeId = value);
  }

  withFirstName(value: string) {
    return this.withProp(o => o.firstName = value);
  }

  withLastName(value: string) {
    return this.withProp(o => o.lastName = value);
  }

  withEmail(value: string) {
    return this.withProp(o => o.email = value);
  }

  withPhone(value: string) {
    return this.withProp(o => o.phone = value);
  }

  withRandomProps() {
    return this.withId(faker.random.alphaNumeric())
      .withTypeId(faker.name.findName())
      .withLastName(faker.name.lastName())
      .withEmail(faker.random.word())
      .withPhone(faker.phone.phoneNumber())
  }
}
