import { CustomerType } from '../interfaces/customerType';
import { TestDataBuilder } from './test.data.builder';
import * as faker from 'faker';

export class CustomerTypeTestDataBuilder extends TestDataBuilder<CustomerTypeTestDataBuilder, CustomerType>{
  constructor() {
    super(() => {
      return {} as CustomerType;
    });
  }
  static create() {
    return new CustomerTypeTestDataBuilder();
  }
  withId(id: string) {
    return this.withProp(o => o.id = id);
  }

  withType(value: string) {
    return this.withProp(o => o.type = value);
  }



  withRandomProps() {
    return this.withId(faker.random.alphaNumeric())
      .withType(faker.random.word())
  }
}
