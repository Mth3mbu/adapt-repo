import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomerListingComponent } from './components/customer-listing/customer-listing.component';
import { CustomerComponent } from './components/customer/customer.component';
import { AppRoutingModule } from './app-routing.module';
import { AppBarComponent } from './components/app-bar/app-bar.component';
import { AngularMaterialModule } from './angular-material.module';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { SpinnerInterceptor } from './interceptors/spinner-interceptor';
import { customerReducer } from './store/reducers/customer.reducer';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CustomerEffects } from './store/effects/customer.effects';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { HttpErrorInterceptor } from './interceptors/http-interceptor';
@NgModule({
  declarations: [
    AppComponent,
    CustomerListingComponent,
    CustomerComponent,
    AppBarComponent,
    SpinnerComponent,
    SnackBarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    AngularMaterialModule,
    StoreModule.forRoot({
      customer: customerReducer
    }),
    EffectsModule.forRoot([
      CustomerEffects
    ])
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }

  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
