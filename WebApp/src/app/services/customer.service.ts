import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Customer } from '../interfaces/customer';
import { CustomerDto } from '../interfaces/customerDto';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }

  getCustomers() {
    return this.http.get<CustomerDto[]>(`${environment.apiUrl}/customer`);
  }

  createCustomer(customer: Customer, total: number) {
    return this.http.post(`${environment.apiUrl}/customer?invoiceBalance=${total}`, customer);
  }

  updateCustomer(customer: Customer, total: number) {
    return this.http.put(`${environment.apiUrl}/customer?invoiceBalance=${total}`, customer);
  }

  deleteCustomer(customerId: string) {
    return this.http.delete(`${environment.apiUrl}/customer?customerId=${customerId}`)
  }
}
