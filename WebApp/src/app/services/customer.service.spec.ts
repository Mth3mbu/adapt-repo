import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CustomerService } from './customer.service';
import { CustomerServiceTestDataBuilder } from '../utest-tils/customer.service.test.data.builder';
import { CustomerDtoDataBuilder } from '../utest-tils/customerDto.testDataBuolder';
import { CustomerTestDataBuilder } from '../utest-tils/customer.testDataBuilder';
import * as faker from 'faker';

describe('CustomerService', () => {
  let service: CustomerService;
  const customers = CustomerDtoDataBuilder.create().withRandomProps().buildList(5)
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = CustomerServiceTestDataBuilder.create().withGetAllCustomers(customers).build();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('it should return all customers', () => {
    service.getCustomers().subscribe(res => {
      expect(res).toBe(customers);
      expect(res.length).toBe(customers.length);
      expect(service.getCustomers).toHaveBeenCalled();
    });
  })

  it('it should create customer', () => {
    //Arrange
    const total = faker.random.number();
    const customer = CustomerTestDataBuilder.create().withRandomProps().build();
    const service = CustomerServiceTestDataBuilder.create().withCreateCustomer(customer).build();
    //Act
    service.createCustomer(customer, total).subscribe(res => {
      //Assert
      expect(res).toBeTruthy();
      expect(service.createCustomer).toHaveBeenCalledWith(customer, total);
      expect(res).toBe(customer);
    });
  })

  it('it should update customer', () => {
    //Arrange
    const total = faker.random.number();
    const customer = CustomerTestDataBuilder.create().withRandomProps().build();
    const service = CustomerServiceTestDataBuilder.create().withUpdate(customer).build();
    //Act
    service.updateCustomer(customer, total).subscribe(res => {
      //Assert
      expect(res).toBeTruthy();
      expect(res).toBe(customer);
      expect(service.updateCustomer).toHaveBeenCalledWith(customer, total);
    });
  })

  it('it should delete customer', () => {
    //Arrange
    const customerId = faker.random.alphaNumeric();
    const service = CustomerServiceTestDataBuilder.create().withDelete(customerId).build();
    //Act
    service.deleteCustomer(customerId).subscribe(res => {
      //Assert
      expect(res).toBeTruthy();
      expect(res).toBe(customerId);
      expect(service.deleteCustomer).toHaveBeenCalledWith(customerId);
    });
  })
});
