import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CustomerTypeService } from './customer-type.service';
import { CostomerTypeServiceTestDataBuilder } from '../utest-tils/customer.ype.service.testDataBuilder';

describe('CustomerTypeService', () => {
  let service: CustomerTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = CostomerTypeServiceTestDataBuilder.create().withGetAllCustomerTypes().build();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should return all customer types', () => {
    service.getCustomerTypes().subscribe(res => {
      expect(res.length).toBe(5);
      expect(service.getCustomerTypes).toHaveBeenCalled();
    })
  })
});
