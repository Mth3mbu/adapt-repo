import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CustomerType } from '../interfaces/customerType';

@Injectable({
  providedIn: 'root'
})
export class CustomerTypeService {

  constructor(private http: HttpClient) { }

  getCustomerTypes() {
    return this.http.get<CustomerType[]>(`${environment.apiUrl}/customer/type`);
  }

}
