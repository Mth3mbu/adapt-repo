﻿using FluentValidation;
using Sales.Models;

namespace Sales.Domain.Validators
{
    public class CustomerValidator : AbstractValidator<Customer>
    {
        public CustomerValidator()
        {
            RuleFor(prop => prop.Email)
                .NotEmpty()
                .NotNull()
                .EmailAddress()
                .NotEmpty()
                .WithMessage("Invalid Email Address");

            RuleFor(prop => prop.FirstName)
                .NotNull()
                .NotEmpty()
                .WithMessage("Please provide First Name");

            RuleFor(prop => prop.LastName)
                .NotNull()
                .NotEmpty()
                .WithMessage("Please provide Last Name");

            RuleFor(prop => prop.Phone)
                 .NotNull()
                 .NotEmpty()
                 .WithMessage("Please provide Last Name");

            RuleFor(prop => prop.Phone)
                 .MinimumLength(10)
                 .MaximumLength(10)
                 .WithMessage("Invalid Phone number");
        }
    }
}
