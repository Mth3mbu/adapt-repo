﻿using Sales.Interfaces.Domain;
using Sales.Interfaces.Intergration.sql;
using Sales.Interfaces.Output;
using Sales.Models;

namespace Sales.Domain
{
    public class CustomerTypeUseCase : ICustomerTypeUseCase
    {
        private readonly ICustomerTypeGateway _customerTypeRepository;
        public CustomerTypeUseCase(ICustomerTypeGateway customerRepository)
        {
            _customerTypeRepository = customerRepository;
        }

        public async Task GetAllCustomerTypes(ISuccessOrErrorPresenter<List<CustomerType>, ErrorDto> presenter)
        {
            var types = await _customerTypeRepository.GetAllTypes();

            presenter.Success(types);
        }
    }
}
