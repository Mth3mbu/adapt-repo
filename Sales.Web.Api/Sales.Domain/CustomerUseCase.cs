﻿using Sales.Interfaces.Domain;
using Sales.Interfaces.Intergration.sql;
using Sales.Interfaces.Output;
using Sales.Models;

namespace Sales.Domain
{
    public class CustomerUseCase : ICustomerUseCase
    {
        private readonly ICustomerGateway _customerRepository;
        private readonly IInvoiceGateway _invoiceRepository;
        public CustomerUseCase(ICustomerGateway customerGateway, IInvoiceGateway invoiceGateway)
        {
            _customerRepository = customerGateway;
            _invoiceRepository = invoiceGateway;
        }

        public async Task CreateCustomer(Customer customer, Decimal InvoiceBalance, IErrorActionResultPresenter<ErrorDto> presenter)
        {

            var exitingEmail = await _customerRepository.GetCustomerByEmail(customer.Email);
            if (exitingEmail != null)
            {
                presenter.Error(CreateErrorMessage($"{customer.Email} Already exists"));
                return;
            }

            var existingPhone = await _customerRepository.GetCustomerByPhone(customer.Phone);
            if (existingPhone != null)
            {
                presenter.Error(CreateErrorMessage($"{customer.Phone} Already exists"));
                return;
            }

            await _customerRepository.CreateCustomer(customer);
            await _invoiceRepository.CreateInvoce(new Invoice
            {
                Balance = InvoiceBalance,
                Id = Guid.NewGuid(),
                UserId = customer.Id
            });
        }

        public async Task DeleteCustomer(Guid customerId, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            var existingCutomer = await _customerRepository.GetCustomerById(customerId);
            if (existingCutomer == null)
            {
                presenter.Error(CreateErrorMessage($"Customer Id {customerId} not found"));
                return;
            }
            await _invoiceRepository.DeleteInvoce(customerId);
            await _customerRepository.DeleteCustomer(customerId);
        }

        public async Task GetAllCustomers(ISuccessOrErrorPresenter<List<CustomerDto>, ErrorDto> presenter)
        {
            var customers = await _customerRepository.GetAllCustomers();

            presenter.Success(customers);
        }

        public async Task UpdateCustomer(Customer customer, Decimal InvoiceBalance, IErrorActionResultPresenter<ErrorDto> presenter)
        {
            await _customerRepository.UpdateCustomer(customer);
            await _invoiceRepository.UpdateteInvoce(new Invoice { Balance = InvoiceBalance, UserId = customer.Id });
        }

        private ErrorDto CreateErrorMessage(string errorMessage)
        {
            return new ErrorDto { Message = errorMessage };
        }
    }
}
