﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Sales.Domain.Output;
using Sales.Interfaces.Domain;
using Sales.Interfaces.Intergration.sql;
using Sales.Models;
using Sales.TestInfrastructure;
using Sales.TestInfrastructure.Customer;
using Sales.TestInfrastructure.Invoices;

namespace Sales.Domain.Tests
{
    [TestFixture]
    internal class CustomerUseCaseTests
    {

        [Test]
        public async Task GivenCustomer_WithExisting_PhoneNumber_ShouldNotCapture()
        {
            // Arrange
            var newCustomer = CustomerTestDataBuilder.BuildRandom();
            var existingCustomer = CustomerTestDataBuilder.BuildRandom();
            existingCustomer.Phone = newCustomer.Phone;
            var customerGateway = CustomerGatewayTestDataBuilder.Create()
                .WithCaptureCreateCustomer(out var captured)
                .WithGetCustomerByPhone(newCustomer.Phone, existingCustomer)
                .Build();
            var sut = CreateService(customerGateway);
            //Act
            await sut.CreateCustomer(newCustomer, RandomPrimitives.RandomMoneyValue(), new ErrorRestPresenter<ErrorDto>());
            //Assert
            captured.Value.Should().BeNull();
        }

        [Test]
        public async Task GivenCustomer_WithExisting_Email_ShouldNotCapture()
        {
            // Arrange
            var newCustomer = CustomerTestDataBuilder.BuildRandom();
            var existingCustomer = CustomerTestDataBuilder.BuildRandom();
            existingCustomer.Email = newCustomer.Email;
            var customerGateway = CustomerGatewayTestDataBuilder.Create()
                .WithCaptureCreateCustomer(out var captured)
                .WithGetCustomerByEmail(newCustomer.Email, existingCustomer)
                .Build();
            var sut = CreateService(customerGateway);
            //Act
            await sut.CreateCustomer(newCustomer, RandomPrimitives.RandomMoneyValue(), new ErrorRestPresenter<ErrorDto>());
            //Assert
            captured.Value.Should().BeNull();
        }

        [Test]
        public async Task GivenCustomer_ShouldCapture()
        {
            // Arrange
            var customer = CustomerTestDataBuilder.BuildRandom();
            var customerGateway = CustomerGatewayTestDataBuilder.Create()
                .WithCaptureCreateCustomer(out var captured)
                .Build();
            var sut = CreateService(customerGateway);
            //Act
            await sut.CreateCustomer(customer, RandomPrimitives.RandomMoneyValue(), new ErrorRestPresenter<ErrorDto>());
            //Assert
            captured.Value.Should().NotBeNull();
        }

        [Test]
        public async Task GivenCustomer_ShouldUpdate()
        {
            // Arrange
            var customer = CustomerTestDataBuilder.BuildRandom();
            var customerGateway = CustomerGatewayTestDataBuilder.Create()
                .WithUpdateCustomer(out var captured)
                .Build();
            var sut = CreateService(customerGateway);
            //Act
            await sut.UpdateCustomer(customer, RandomPrimitives.RandomMoneyValue(), new ErrorRestPresenter<ErrorDto>());
            //Assert
            captured.Value.Should().NotBeNull();
        }

        [Test]
        public async Task GivenCustomerId_ShouldRetunCustomer()
        {
            // Arrange
            var customer = CustomerTestDataBuilder.BuildRandom();
            var customerGateway = CustomerGatewayTestDataBuilder.Create()
                .WithUpdateCustomer(out var captured)
                .Build();
            var sut = CreateService(customerGateway);
            //Act
            await sut.UpdateCustomer(customer, RandomPrimitives.RandomMoneyValue(), new ErrorRestPresenter<ErrorDto>());
            //Assert
            captured.Value.Should().NotBeNull();
            captured.Value.Email.Should().Be(customer.Email);
            captured.Value.Phone.Should().Be(customer.Phone);
            captured.Value.Id.Should().Be(customer.Id);
            captured.Value.TypeId.Should().Be(customer.TypeId);
            captured.Value.FirstName.Should().Be(customer.FirstName);
            captured.Value.LastName.Should().Be(customer.LastName);
        }

        [Test]
        public async Task ShouldReturnAllCustomers()
        {
            //Arrange
            var customers = CustomerDtoTestDataBuilder.BuildRandomCollection(count: 10).ToList();
            var customerRepository = CustomerGatewayTestDataBuilder.Create()
              .WithGetAllCustomers(customers)
              .Build();
            var sut = CreateService(customerRepository);
            var pressenter = new SuccessOrErrorRestPresenter<List<CustomerDto>, ErrorDto>();
            //Act
            await sut.GetAllCustomers(pressenter);
            var response = pressenter.Render() as OkObjectResult;
            //Assert
            response?.Value.Should().NotBeNull();
            (response?.Value as List<CustomerDto>).Count().Should().Be(10);
        }


        private static ICustomerUseCase CreateService(ICustomerGateway customerGateway)
        {
            var invoiceRepository = InvoiceGatewayTestDataBuilder.Create().Build();
            return new CustomerUseCase(customerGateway, invoiceRepository);
        }
    }
}
