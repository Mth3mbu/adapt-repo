using FluentMigrator.Runner;
using FluentValidation.AspNetCore;
using Sales.Domain.Validators;
using Sales.Web.Api.Infrastructure;
using SalesApi.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSqlIntegration()
    .AddMigrations()
    .AddUseCases()
    .AddPresenters()
    .AddFluentValidation(config =>
{
    config.DisableDataAnnotationsValidation = true;
    config.RegisterValidatorsFromAssemblyContaining<CustomerValidator>();
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

RunDbMigrator(app);

SetCors(app, app.Configuration);

app.Run();

void RunDbMigrator(IApplicationBuilder app)
{
    using var startupScope = app.ApplicationServices.CreateScope();
    var migrationRunner = startupScope.ServiceProvider.GetService<IMigrationRunner>();
    migrationRunner?.MigrateUp();
}

void SetCors(IApplicationBuilder app, IConfiguration configuration)
{
    var corsSettings = configuration.Read<CorsSettings>("Cors");
    app.UseCors(builder => builder
        .WithOrigins(corsSettings.Origins)
        .AllowAnyHeader()
        .AllowAnyMethod()
    );
}