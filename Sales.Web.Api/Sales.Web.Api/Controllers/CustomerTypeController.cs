﻿using Microsoft.AspNetCore.Mvc;
using Sales.Interfaces.Domain;
using Sales.Interfaces.Output;
using Sales.Models;

namespace Sales.Web.Api.Controllers
{
    [ApiController]
    [Route("customer/type")]
    public class CustomerTypeController : ControllerBase
    {
        private readonly ICustomerTypeUseCase _useCase;
        private readonly ISuccessOrErrorActionResultPresenter<List<CustomerType>, ErrorDto> _presenter;
        public CustomerTypeController(ICustomerTypeUseCase useCase,
             ISuccessOrErrorActionResultPresenter<List<CustomerType>, ErrorDto> presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCustomers()
        {
            await _useCase.GetAllCustomerTypes(_presenter);

            return _presenter.Render();
        }
    }
}
