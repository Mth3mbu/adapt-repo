﻿using Microsoft.AspNetCore.Mvc;
using Sales.Interfaces.Domain;
using Sales.Interfaces.Output;
using Sales.Models;

namespace Sales.Web.Api.Controllers
{
    [ApiController]
    [Route("customer")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerUseCase _useCase;
        private readonly ISuccessOrErrorActionResultPresenter<List<CustomerDto>, ErrorDto> _customerPresenter;
        private readonly IErrorActionResultPresenter<ErrorDto> _ErrorPressenter;
        public CustomerController(ICustomerUseCase useCase,
             ISuccessOrErrorActionResultPresenter<List<CustomerDto>, ErrorDto> customerPresenter,
             IErrorActionResultPresenter<ErrorDto> presenter)
        {
            _useCase = useCase;
            _customerPresenter = customerPresenter;
            _ErrorPressenter = presenter;
        }

        [HttpPost]
        public async Task<IActionResult> Create(Customer customer, Decimal invoiceBalance)
        {
            await _useCase.CreateCustomer(customer, invoiceBalance, _ErrorPressenter);

            return _ErrorPressenter.Render();
        }

        [HttpPut]
        public async Task<IActionResult> Update(Customer customer, Decimal invoiceBalance)
        {
            await _useCase.UpdateCustomer(customer, invoiceBalance, _ErrorPressenter);

            return _ErrorPressenter.Render();
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid customerId)
        {
            await _useCase.DeleteCustomer(customerId, _ErrorPressenter);

            return _ErrorPressenter.Render();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCustomers()
        {
            await _useCase.GetAllCustomers(_customerPresenter);

            return _customerPresenter.Render();
        }
    }
}
