﻿namespace SalesApi.Infrastructure
{
    using global::Sales.Interfaces.Connection;
    using Microsoft.Extensions.Configuration;

    namespace Sales.Web.Main.Infrastructure
    {
        public class DbSettings : IDbSettings
        {
            private readonly IConfiguration _configuration;
            public DbSettings(IConfiguration configuration)
            {
                _configuration = configuration;
            }

            public string ConnectionString => _configuration.GetConnectionString("SalesDb");
        }
    }

}

