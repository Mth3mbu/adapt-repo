﻿namespace SalesApi.Infrastructure
{
    public class CorsSettings
    {
        public string[]? Origins { get; set; }
    }
}
