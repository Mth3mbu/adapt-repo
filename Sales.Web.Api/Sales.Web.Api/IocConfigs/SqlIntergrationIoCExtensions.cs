﻿using Sales.Interfaces.Connection;
using Sales.Interfaces.Intergration.sql;
using Sales.Intergration.sql;
using Sales.Intergration.sql.Connection;
using SalesApi.Infrastructure.Sales.Web.Main.Infrastructure;

namespace Sales.Web.Api.Infrastructure
{
    public static class SqlIntergrationIoCExtensions
    {
        public static IServiceCollection AddSqlIntegration(this IServiceCollection services)
        {
            services.AddScoped<IDbSettings, DbSettings>();
            services.AddScoped<ISalesDbConnectionContext>(
                provider => new AutoConnectingDbConnectionContext(provider.GetService<IDbSettings>(), settings => settings.ConnectionString));

            services.AddScoped<ICustomerGateway, CustomerGateway>();
            services.AddScoped<IInvoiceGateway, InvoiceGateway>();
            services.AddScoped<ICustomerTypeGateway, CustomerTypeGateway>();

            return services;
        }
    }
}
