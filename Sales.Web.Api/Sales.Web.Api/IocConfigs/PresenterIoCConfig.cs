﻿using Sales.Domain.Output;
using Sales.Interfaces.Output;

namespace Sales.Web.Api.Infrastructure
{
    public static class PresenterIoCConfig
    {
        public static IServiceCollection AddPresenters(this IServiceCollection services)
        {
            services.AddScoped(typeof(ISuccessOrErrorActionResultPresenter<,>), typeof(SuccessOrErrorRestPresenter<,>));
            services.AddScoped(typeof(IErrorActionResultPresenter<>), typeof(ErrorRestPresenter<>));

            return services;
        }
    }
}
