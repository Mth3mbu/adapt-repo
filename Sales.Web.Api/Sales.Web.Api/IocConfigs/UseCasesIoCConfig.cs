﻿using Sales.Domain;
using Sales.Interfaces.Domain;

namespace Sales.Web.Api.Infrastructure
{
    public static class UseCasesIoCConfig
    {
        public static IServiceCollection AddUseCases(this IServiceCollection services)
        {
            services.AddScoped<ICustomerUseCase, CustomerUseCase>();
            services.AddScoped<ICustomerTypeUseCase, CustomerTypeUseCase>();

            return services;
        }
    }
}
