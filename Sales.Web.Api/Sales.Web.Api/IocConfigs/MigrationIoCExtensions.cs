﻿using FluentMigrator.Runner;
using Sales.Interfaces.Connection;
using Sales.Repositories.Migrations;

namespace Sales.Web.Api.Infrastructure
{
    public static class MigrationIoCExtensions
    {
        public static IServiceCollection AddMigrations(this IServiceCollection services)
        {
            services.AddTransient(provider =>
            {
                var dbSettings = provider.GetService<IDbSettings>();
                var serviceProvider = (IServiceProvider)new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb.AddSqlServer()
                                        .WithGlobalConnectionString(dbSettings?.ConnectionString)
                                        .ScanIn(typeof(IMigrationMarker).Assembly).For.All())
                .AddLogging(lb => lb.AddFluentMigratorConsole()).BuildServiceProvider(false);

                return serviceProvider.GetRequiredService<IMigrationRunner>();
            });

            return services;
        }
    }
}
