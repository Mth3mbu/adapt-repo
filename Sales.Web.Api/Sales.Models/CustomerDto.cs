﻿namespace Sales.Models
{
    public class CustomerDto
    {
        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public Decimal Balance { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
