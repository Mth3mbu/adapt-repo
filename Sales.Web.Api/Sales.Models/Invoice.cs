﻿namespace Sales.Models
{
    public class Invoice
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Decimal Balance { get; set; }
    }
}
