﻿ namespace Sales.Models
{
    public class ErrorDto
    {
        public string Message { get; set; }
    }
}
