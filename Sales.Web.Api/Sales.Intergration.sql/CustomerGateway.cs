﻿using Dapper;
using Sales.Interfaces.Connection;
using Sales.Interfaces.Intergration.sql;
using System.Data;

namespace Sales.Intergration.sql
{
    public class CustomerGateway : ICustomerGateway
    {
        private readonly ISalesDbConnectionContext _context;
        private IDbTransaction DbTransaction => _context.GetTransaction();
        private IDbConnection DbConnection => _context.GetConnection();
        public CustomerGateway(ISalesDbConnectionContext salesDbConnectionContext)
        {
            _context = salesDbConnectionContext;
        }

        public async Task<bool> CreateCustomer(Models.Customer customer)
        {
            var query = @"INSERT INTO [Customer]
                                               ([Id]
                                               ,[TypeId]
                                               ,[FirstName]
                                               ,[LastName]
                                               ,[Email]
                                               ,[Phone])
                                                VALUES (@Id,@TypeId, @FirstName,@LastName,@Email,@Phone)";

            return await DbConnection.ExecuteAsync(query, customer, DbTransaction) == 1;
        }

        public async Task<bool> UpdateCustomer(Models.Customer customer)
        {
            return await DbConnection.ExecuteAsync("spUpdateCustomer", customer, commandType: CommandType.StoredProcedure, transaction: DbTransaction) == 1;
        }

        public async Task<bool> DeleteCustomer(Guid customerId)
        {
            var query = @"DELETE FROM [Customer] WHERE Id=@customerId";

            return await DbConnection.ExecuteAsync(query, new { customerId }, DbTransaction) == 1;
        }

        public async Task<Models.Customer> GetCustomerByPhone(string phone)
        {
            var query = @"SELECT [Id]
                                  ,[TypeId]
                                  ,[FirstName]
                                  ,[LastName]
                                  ,[Email]
                                  ,[Phone]
                              FROM [Customer] WHERE Phone=@phone";

            return await DbConnection.QueryFirstOrDefaultAsync<Models.Customer>(query, new { phone }, transaction: DbTransaction);
        }

        public async Task<Models.Customer> GetCustomerByEmail(string email)
        {
            var query = @"SELECT [Id]
                                  ,[TypeId]
                                  ,[FirstName]
                                  ,[LastName]
                                  ,[Email]
                                  ,[Phone]
                              FROM [Customer] WHERE Email=@email";

            return await DbConnection.QueryFirstOrDefaultAsync<Models.Customer>(query, new { email }, DbTransaction);
        }

        public async Task<Models.Customer> GetCustomerById(Guid customerId)
        {
            var query = @"SELECT [Id]
                                  ,[TypeId]
                                  ,[FirstName]
                                  ,[LastName]
                                  ,[Email]
                                  ,[Phone]
                              FROM [Customer] WHERE Id=@customerId";

            return await DbConnection.QueryFirstOrDefaultAsync<Models.Customer>(query, new { customerId }, DbTransaction);
        }

        public async Task<List<Models.CustomerDto>> GetAllCustomers()
        {
            var query = @"SELECT c.[Id]
                                  ,[TypeId]
	                              ,i.[Balance]
                                  ,[FirstName]
                                  ,[LastName]
                                  ,[Email]
                                  ,[Phone]
                              FROM [Customer] c LEFT JOIN [Invoice] i ON c.[Id]=i.[UserId]";

            var customers = await DbConnection.QueryAsync<Models.CustomerDto>(query, transaction: DbTransaction);

            return customers.ToList();
        }
    }
}
