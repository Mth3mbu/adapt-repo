﻿using FluentMigrator;

namespace Sales.Intergration.sql.Migrations
{
    [Migration(202112042143)]
    public class M007_CreateUpdateInvoiceProc : Migration
    {
        public override void Down()
        {
           
        }

        public override void Up()
        {
            Execute.Sql(@"CREATE PROC spUpdateInvoice
                                            @UserId uniqueidentifier,
                                            @Balance decimal
                                            AS
                                            BEGIN
                                            UPDATE[dbo].[Invoice]
                                               SET[Balance] = @Balance
                                             WHERE[UserId] = @UserId
                                            END");
        }
    }
}
