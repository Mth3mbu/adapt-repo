﻿using FluentMigrator;

namespace Sales.Intergration.sql.Migrations
{
    [Migration(202112031015)]
    public class M004_InsertCustomerTypeTable : Migration
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Insert.IntoTable("CustomerType").Row(new
            {
                Id = "d01f284a-3f58-43e6-8d39-c03f9d4aac66",
                Type = "Gold"
            });

            Insert.IntoTable("CustomerType").Row(new
            {
                Id = "d96f0a1f-126a-4a1a-9ad9-0ba619e03198",
                Type = "Silver"
            });

            Insert.IntoTable("CustomerType").Row(new
            {
                Id = "d74ab79f-3da4-45fa-849c-1c7621ba0cbc",
                Type = "Platinum"
            });
        }
    }
}
