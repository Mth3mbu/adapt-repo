﻿using FluentMigrator;

namespace Sales.Intergration.sql.Migrations
{
    [Migration(202112042135)]
    public class M006_CreateInsertInvoiceProc : Migration
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Execute.Sql(@"CREATE PROC spInsertInvoice
                                           @Id uniqueidentifier,
                                           @UserId uniqueidentifier,
                                           @Balance decimal
                                        AS
                                        BEGIN
                                        INSERT INTO[Invoice]
                                                   ([Id]
                                                   ,[UserId]
                                                   ,[Balance])
                                             VALUES
                                                  (@Id
                                                  , @UserId
                                                  , @Balance)
                                        END");
        }
    }
}
