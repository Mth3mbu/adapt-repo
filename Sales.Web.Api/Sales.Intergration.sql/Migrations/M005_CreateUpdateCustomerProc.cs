﻿using FluentMigrator;

namespace Sales.Intergration.sql.Migrations
{
    [Migration(202112042103)]
    public class M005_CreateUpdateCustomerProc : Migration
    {
        public override void Down()
        {
          
        }

        public override void Up()
        {
            var sql = @"CREATE PROC spUpdateCustomer
                                    @Id uniqueidentifier,
                                    @TypeId uniqueidentifier,
                                    @FirstName nvarchar(150),
                                    @LastName nvarchar(150),
                                    @Email nvarchar(100),
                                    @Phone nvarchar(10)
                                    AS
                                    BEGIN
                                    UPDATE [Customer]
                                       SET [Id] = @Id
                                          ,[TypeId] = @TypeId
                                          ,[FirstName] = @FirstName
                                          ,[LastName] = @LastName
                                          ,[Email] = @Email
                                          ,[Phone] = @Phone
                                     WHERE [Id] = @Id
                                    END";
            Execute.Sql(sql);
        }
    }
}
