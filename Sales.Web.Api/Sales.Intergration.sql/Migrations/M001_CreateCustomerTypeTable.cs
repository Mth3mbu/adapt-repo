﻿using FluentMigrator;

namespace Sales.Intergration.sql.Migrations
{
    [Migration(202112031012)]
    public class M001_CreateCustomerTypeTable : Migration
    {
        public override void Down()
        {
         
        }

        public override void Up()
        {
            Create.Table("CustomerType")
              .WithColumn("Id").AsGuid().PrimaryKey()
              .WithColumn("Type").AsString(150);
        }
    }
}
