﻿using FluentMigrator;

namespace Sales.Intergration.sql.Migrations
{
    [Migration(202112031013)]
    public class M002_CreateCustomerTable : Migration
    {
        public override void Down()
        {

        }

        public override void Up()
        {
            Create.Table("Customer")
               .WithColumn("Id").AsGuid().PrimaryKey()
                .WithColumn("TypeId").AsGuid().ForeignKey("fk_customerTypeId", "CustomerType", "Id")
               .WithColumn("FirstName").AsString(150).NotNullable()
                .WithColumn("LastName").AsString(150)
                .WithColumn("Email").AsString(100)
               .WithColumn("Phone").AsString(10);
        }
    }
}
