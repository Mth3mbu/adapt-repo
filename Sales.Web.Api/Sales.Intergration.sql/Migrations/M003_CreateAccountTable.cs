﻿using FluentMigrator;

namespace Sales.Intergration.sql.Migrations
{
    [Migration(202112031014)]
    public class M003_CreateAccountTable : Migration
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Create.Table("Invoice")
                .WithColumn("Id").AsGuid().PrimaryKey()
                 .WithColumn("UserId").AsGuid().ForeignKey("fk_customerId","Customer", "Id")
                .WithColumn("Balance").AsDecimal();
        }
    }
}
