﻿using Dapper;
using Sales.Interfaces.Connection;
using Sales.Interfaces.Intergration.sql;
using System.Data;

namespace Sales.Intergration.sql
{
    public class InvoiceGateway: IInvoiceGateway
    {
        private readonly ISalesDbConnectionContext _context;
        private IDbTransaction DbTransaction => _context.GetTransaction();
        private IDbConnection DbConnection => _context.GetConnection();
        public InvoiceGateway(ISalesDbConnectionContext salesDbConnectionContext)
        {
            _context = salesDbConnectionContext;
        }
        public async Task CreateInvoce(Models.Invoice invoice)
        {
            await DbConnection.ExecuteAsync("spInsertInvoice", invoice, commandType: CommandType.StoredProcedure, transaction: DbTransaction);
        }

        public async Task UpdateteInvoce(Models.Invoice invoice)
        {
            await DbConnection.ExecuteAsync("spUpdateInvoice", new
            {
                UserId = invoice.UserId,
                Balance = invoice.Balance
            }, commandType: CommandType.StoredProcedure, transaction: DbTransaction);
        }

        public async Task DeleteInvoce(Guid userId)
        {
            var query = @"DELETE [Invoice] WHERE [UserId] = @userId";

            await DbConnection.ExecuteAsync(query, new { userId }, DbTransaction);
        }
    }
}
