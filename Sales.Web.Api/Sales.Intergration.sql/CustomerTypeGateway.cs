﻿using Dapper;
using Sales.Interfaces.Connection;
using Sales.Interfaces.Intergration.sql;
using Sales.Models;
using System.Data;

namespace Sales.Intergration.sql
{
    public class CustomerTypeGateway: ICustomerTypeGateway
    {
        private readonly ISalesDbConnectionContext _context;
        private IDbTransaction DbTransaction => _context.GetTransaction();
        private IDbConnection DbConnection => _context.GetConnection();
        public CustomerTypeGateway(ISalesDbConnectionContext salesDbConnectionContext)
        {
            _context = salesDbConnectionContext;
        }

        public async Task<List<CustomerType>> GetAllTypes()
        {
            var query = @"SELECT [Id]
                                ,[Type]
                              FROM [CustomerType]";

            var customers = await DbConnection.QueryAsync<CustomerType>(query, transaction: DbTransaction);

            return customers.ToList();
        }
    }
}
