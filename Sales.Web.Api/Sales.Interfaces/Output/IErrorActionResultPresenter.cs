﻿using Microsoft.AspNetCore.Mvc;

namespace Sales.Interfaces.Output
{
    public interface IErrorActionResultPresenter<in TError> : IErrorPresenter<TError>
    {
        public IActionResult Render();
    }
}
