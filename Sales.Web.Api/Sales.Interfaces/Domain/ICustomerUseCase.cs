﻿using Sales.Interfaces.Output;
using Sales.Models;

namespace Sales.Interfaces.Domain
{
    public interface ICustomerUseCase
    {
        Task CreateCustomer(Customer customer, Decimal InvoiceBalance, IErrorActionResultPresenter<ErrorDto> presenter);
        Task UpdateCustomer(Customer customer, Decimal InvoiceBalance, IErrorActionResultPresenter<ErrorDto> presenter);
        Task DeleteCustomer(Guid customerId, IErrorActionResultPresenter<ErrorDto> presenter);
        Task GetAllCustomers(ISuccessOrErrorPresenter<List<CustomerDto>, ErrorDto> presenter);
    }
}
