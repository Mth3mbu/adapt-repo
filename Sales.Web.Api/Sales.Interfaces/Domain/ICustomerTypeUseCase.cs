﻿using Sales.Interfaces.Output;
using Sales.Models;


namespace Sales.Interfaces.Domain
{
    public interface ICustomerTypeUseCase
    {
        Task GetAllCustomerTypes(ISuccessOrErrorPresenter<List<CustomerType>, ErrorDto> presenter);  
    }
}
