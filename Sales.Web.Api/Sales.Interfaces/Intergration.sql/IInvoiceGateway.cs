﻿using Sales.Models;

namespace Sales.Interfaces.Intergration.sql
{
    public interface IInvoiceGateway
    {
        Task CreateInvoce(Invoice invoice);
        Task UpdateteInvoce(Invoice invoice);
        Task DeleteInvoce(Guid userId);
    }
}
