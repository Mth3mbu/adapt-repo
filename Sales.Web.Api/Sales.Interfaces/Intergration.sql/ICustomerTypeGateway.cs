﻿using Sales.Models;

namespace Sales.Interfaces.Intergration.sql
{
    public interface ICustomerTypeGateway
    {
        Task<List<CustomerType>> GetAllTypes();
    }
}
