﻿using Sales.Models;

namespace Sales.Interfaces.Intergration.sql
{
    public interface ICustomerGateway
    {
        Task<bool> CreateCustomer(Customer customer);
        Task<bool> UpdateCustomer(Customer customer);
        Task<bool> DeleteCustomer(Guid customerId);
        Task<List<CustomerDto>> GetAllCustomers();
        Task<Customer> GetCustomerByPhone(string phone);
        Task<Customer> GetCustomerById(Guid customerId);
        Task<Customer> GetCustomerByEmail(string email);
    }
}
