﻿namespace Sales.Interfaces.Connection
{
    public interface IDbSettings
    {
        string ConnectionString { get; }
    }
}
