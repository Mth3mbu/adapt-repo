﻿using System.Data;

namespace Sales.Interfaces.Connection
{
    public interface IDbConnectionContext
    {
        IDbConnection GetConnection();
        IDbTransaction GetTransaction();
        void Commit();
        void Rollback();
    }
}
