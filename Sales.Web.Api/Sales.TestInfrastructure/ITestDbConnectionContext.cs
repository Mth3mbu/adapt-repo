﻿using System.Data;

namespace Sales.TestInfrastructure
{
    public interface ITestDbConnectionContext
    {
        IDbConnection Connection { get; set; }
        IDbTransaction Transaction { get; set; }
    }
}
