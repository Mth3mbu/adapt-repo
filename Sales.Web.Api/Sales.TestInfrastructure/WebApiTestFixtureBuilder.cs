﻿using System.Text;
using FluentMigrator.Runner;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using NSubstitute;
using SalesApi.Infrastructure;

namespace Sales.TestInfrastructure
{
    public class WebApiTestFixtureBuilder : TestDataBuilder<WebApiTestFixtureBuilder, WebApiTestFixture>
    {
        private readonly List<Action<IServiceCollection>> _dependencyMutations = new List<Action<IServiceCollection>>();
        private readonly Dictionary<string, object> _testConfiguration = new Dictionary<string, object>();


        public WebApiTestFixtureBuilder()
        {
            WithCorsSettings(new CorsSettings { Origins = new[] { "*" } });
        }

        public WebApiTestFixtureBuilder WithScopedDependency<T>(T instance)
        {
            _dependencyMutations.Add(services => services.SwapScoped(instance));

            return this;
        }

        public WebApiTestFixtureBuilder WithTransientDependency<T>(T instance)
        {
            _dependencyMutations.Add(services => services.SwapTransient(instance));

            return this;
        }

        public WebApiTestFixtureBuilder WithCorsSettings(CorsSettings value)
        {
            _testConfiguration["Cors"] = value;

            return this;
        }

        public override WebApiTestFixture Build(Action<WebApiTestFixtureBuilder> alterationsForThisBuild = null)
        {
            WithTransientDependency(Substitute.For<IMigrationRunner>());
            WithProp(o => o.Host = CreateMainHost());

            return base.Build(alterationsForThisBuild);
        }

        private IHost CreateMainHost()
        {
            return new HostBuilder()
                .ConfigureWebHost(webHost =>
                {
                    webHost.UseTestServer();
                    webHost.ConfigureTestServices(services =>
                    {
                        foreach (var mutation in _dependencyMutations)
                        {
                            mutation.Invoke(services);
                        }
                    });
                })
                .ConfigureAppConfiguration(builder =>
                {
                    builder.AddJsonStream(
                        new MemoryStream(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(_testConfiguration)))
                    );
                })
                .StartAsync()
                .Result;
        }
    }
}
