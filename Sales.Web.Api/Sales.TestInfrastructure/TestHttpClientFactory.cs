﻿using Flurl.Http.Configuration;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;

namespace Sales.TestInfrastructure
{
    public class TestHttpClientFactory : DefaultHttpClientFactory
    {
        private readonly IHost _host;

        public TestHttpClientFactory(IHost host)
        {
            _host = host;
        }

        public override HttpClient CreateHttpClient(HttpMessageHandler handler)
        {
            return _host.GetTestClient();
        }

        public override HttpMessageHandler CreateMessageHandler()
        {
            return _host.GetTestServer().CreateHandler();
        }
    }
}
