﻿using NSubstitute;
using NSubstitute.Core;
namespace Sales.TestInfrastructure
{

    public abstract class MethodCapture
    {
        /// <summary>
        /// Gets a value indicating if the captured method has been called.
        /// </summary>
        public bool Invoked { get; private set; }

        protected virtual void NotifyTargetInvoked(CallInfo info)
        {
            Invoked = true;
        }
    }

    public abstract class MethodCapture<T1> : MethodCapture
    {
        protected readonly Captured<T1> _a1;

        protected MethodCapture()
        {
            _a1 = new Captured<T1>();
        }

        protected override void NotifyTargetInvoked(CallInfo info)
        {
            base.NotifyTargetInvoked(info);
            _a1.SetValue(info.ArgAt<T1>(0));
        }

        public void Deconstruct(out Captured<T1> a1)
        {
            a1 = _a1;
        }
    }

    public abstract class MethodCapture<T1, T2> : MethodCapture<T1>
    {
        protected readonly Captured<T2> _a2;

        protected MethodCapture()
        {
            _a2 = new Captured<T2>();
        }

        protected override void NotifyTargetInvoked(CallInfo info)
        {
            base.NotifyTargetInvoked(info);
            _a2.SetValue(info.ArgAt<T2>(1));
        }

        public void Deconstruct(out Captured<T1> a1, out Captured<T2> a2)
        {
            a1 = _a1;
            a2 = _a2;
        }
    }

    public abstract class MethodCapture<T1, T2, T3> : MethodCapture<T1, T2>
    {
        protected readonly Captured<T3> _a3;

        protected MethodCapture()
        {
            _a3 = new Captured<T3>();
        }

        protected override void NotifyTargetInvoked(CallInfo info)
        {
            base.NotifyTargetInvoked(info);
            _a3.SetValue(info.ArgAt<T3>(2));
        }

        public void Deconstruct(out Captured<T1> a1, out Captured<T2> a2, out Captured<T3> a3)
        {
            a1 = _a1;
            a2 = _a2;
            a3 = _a3;
        }
    }

    public abstract class MethodCapture<T1, T2, T3, T4> : MethodCapture<T1, T2, T3>
    {
        protected readonly Captured<T4> _a4;

        protected MethodCapture()
        {
            _a4 = new Captured<T4>();
        }

        protected override void NotifyTargetInvoked(CallInfo info)
        {
            base.NotifyTargetInvoked(info);
            _a4.SetValue(info.ArgAt<T4>(3));
        }

        public void Deconstruct(out Captured<T1> a1, out Captured<T2> a2, out Captured<T3> a3, out Captured<T4> a4)
        {
            a1 = _a1;
            a2 = _a2;
            a3 = _a3;
            a4 = _a4;
        }
    }

    public abstract class MethodCapture<T1, T2, T3, T4, T5> : MethodCapture<T1, T2, T3, T4>
    {
        protected readonly Captured<T5> _a5;

        protected MethodCapture()
        {
            _a5 = new Captured<T5>();
        }

        protected override void NotifyTargetInvoked(CallInfo info)
        {
            base.NotifyTargetInvoked(info);
            _a5.SetValue(info.ArgAt<T5>(4));
        }

        public void Deconstruct(
          out Captured<T1> a1,
          out Captured<T2> a2,
          out Captured<T3> a3,
          out Captured<T4> a4,
          out Captured<T5> a5)
        {
            a1 = _a1;
            a2 = _a2;
            a3 = _a3;
            a4 = _a4;
            a5 = _a5;
        }
    }

    public abstract class MethodCapture<T1, T2, T3, T4, T5, T6> : MethodCapture<T1, T2, T3, T4, T5>
    {
        protected readonly Captured<T6> _a6;

        protected MethodCapture()
        {
            _a6 = new Captured<T6>();
        }

        protected override void NotifyTargetInvoked(CallInfo info)
        {
            base.NotifyTargetInvoked(info);
            _a6.SetValue(info.ArgAt<T6>(5));
        }

        public void Deconstruct(
          out Captured<T1> a1,
          out Captured<T2> a2,
          out Captured<T3> a3,
          out Captured<T4> a4,
          out Captured<T5> a5,
          out Captured<T6> a6)
        {
            a1 = _a1;
            a2 = _a2;
            a3 = _a3;
            a4 = _a4;
            a5 = _a5;
            a6 = _a6;
        }
    }

    public abstract class MethodCapture<T1, T2, T3, T4, T5, T6, T7> : MethodCapture<T1, T2, T3, T4, T5, T6>
    {
        protected readonly Captured<T7> _a7;

        protected MethodCapture()
        {
            _a7 = new Captured<T7>();
        }

        protected override void NotifyTargetInvoked(CallInfo info)
        {
            base.NotifyTargetInvoked(info);
            _a7.SetValue(info.ArgAt<T7>(6));
        }

        public void Deconstruct(
          out Captured<T1> a1,
          out Captured<T2> a2,
          out Captured<T3> a3,
          out Captured<T4> a4,
          out Captured<T5> a5,
          out Captured<T6> a6,
          out Captured<T7> a7)
        {
            a1 = _a1;
            a2 = _a2;
            a3 = _a3;
            a4 = _a4;
            a5 = _a5;
            a6 = _a6;
            a7 = _a7;
        }
    }

    public abstract class MethodCapture<T1, T2, T3, T4, T5, T6, T7, T8> : MethodCapture<T1, T2, T3, T4, T5, T6, T7>
    {
        private readonly Captured<T8> _a8;

        protected MethodCapture()
        {
            _a8 = new Captured<T8>();
        }

        protected override void NotifyTargetInvoked(CallInfo info)
        {
            base.NotifyTargetInvoked(info);
            _a8.SetValue(info.ArgAt<T8>(7));
        }

        public void Deconstruct(
          out Captured<T1> a1,
          out Captured<T2> a2,
          out Captured<T3> a3,
          out Captured<T4> a4,
          out Captured<T5> a5,
          out Captured<T6> a6,
          out Captured<T7> a7,
          out Captured<T8> a8)
        {
            a1 = _a1;
            a2 = _a2;
            a3 = _a3;
            a4 = _a4;
            a5 = _a5;
            a6 = _a6;
            a7 = _a7;
            a8 = _a8;
        }
    }

    public class InstanceMethodCapture<TInstance> : MethodCapture where TInstance : class
    {
        public void Bind(TInstance instance, Action target)
        {
            instance.When(o => target())
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Func<Task> target)
        {
            instance.When(o => target())
             .Do(NotifyTargetInvoked);
        }
    }

    public class InstanceMethodCapture<TInstance, T1> : MethodCapture<T1> where TInstance : class
    {
        public void BindFunc<TReturnType>(TInstance instance, Func<T1, TReturnType> target)
        {
            instance.When(o => target(Arg.Any<T1>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Action<T1> target)
        {
            instance.When(o => target(Arg.Any<T1>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Func<T1, Task> target)
        {
            instance.When(o => target(Arg.Any<T1>()))
             .Do(NotifyTargetInvoked);
        }
    }

    public class InstanceMethodCapture<TInstance, T1, T2> : MethodCapture<T1, T2> where TInstance : class
    {
        public void BindFunc<TReturnType>(TInstance instance, Func<T1, T2, TReturnType> target)
        {
            instance.When(o => target(Arg.Any<T1>(), Arg.Any<T2>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Action<T1, T2> target)
        {
            instance.When(o => target(Arg.Any<T1>(), Arg.Any<T2>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Func<T1, T2, Task> target)
        {
            instance.When(o => target(Arg.Any<T1>(), Arg.Any<T2>()))
             .Do(NotifyTargetInvoked);
        }
    }

    public class InstanceMethodCapture<TInstance, T1, T2, T3> : MethodCapture<T1, T2, T3> where TInstance : class
    {
        public void Bind(TInstance instance, Action<T1, T2, T3> target)
        {
            instance.When(o => target(Arg.Any<T1>(), Arg.Any<T2>(), Arg.Any<T3>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Func<T1, T2, T3, Task> target)
        {
            instance.When(o => target(Arg.Any<T1>(), Arg.Any<T2>(), Arg.Any<T3>()))
             .Do(NotifyTargetInvoked);
        }
    }

    public class InstanceMethodCapture<TInstance, T1, T2, T3, T4> : MethodCapture<T1, T2, T3, T4> where TInstance : class
    {
        public void Bind(TInstance instance, Action<T1, T2, T3, T4> target)
        {
            instance.When(o => target(Arg.Any<T1>(), Arg.Any<T2>(), Arg.Any<T3>(), Arg.Any<T4>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Func<T1, T2, T3, T4, Task> target)
        {
            instance.When(o => target(Arg.Any<T1>(), Arg.Any<T2>(), Arg.Any<T3>(), Arg.Any<T4>()))
             .Do(NotifyTargetInvoked);
        }
    }

    public class InstanceMethodCapture<TInstance, T1, T2, T3, T4, T5> : MethodCapture<T1, T2, T3, T4, T5>
      where TInstance : class
    {
        public void Bind(TInstance instance, Action<T1, T2, T3, T4, T5> target)
        {
            instance.When(o => target(Arg.Any<T1>(), Arg.Any<T2>(), Arg.Any<T3>(), Arg.Any<T4>(), Arg.Any<T5>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Func<T1, T2, T3, T4, T5, Task> target)
        {
            instance.When(o => target(Arg.Any<T1>(), Arg.Any<T2>(), Arg.Any<T3>(), Arg.Any<T4>(), Arg.Any<T5>()))
             .Do(NotifyTargetInvoked);
        }
    }

    public class InstanceMethodCapture<TInstance, T1, T2, T3, T4, T5, T6> : MethodCapture<T1, T2, T3, T4, T5, T6>
      where TInstance : class
    {
        public void Bind(TInstance instance, Action<T1, T2, T3, T4, T5, T6> target)
        {
            instance.When(o => target(Arg.Any<T1>(),
                Arg.Any<T2>(),
                Arg.Any<T3>(),
                Arg.Any<T4>(),
                Arg.Any<T5>(),
                Arg.Any<T6>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Func<T1, T2, T3, T4, T5, T6, Task> target)
        {
            instance.When(o => target(Arg.Any<T1>(),
                Arg.Any<T2>(),
                Arg.Any<T3>(),
                Arg.Any<T4>(),
                Arg.Any<T5>(),
                Arg.Any<T6>()))
             .Do(NotifyTargetInvoked);
        }
    }

    public class InstanceMethodCapture<TInstance, T1, T2, T3, T4, T5, T6, T7> : MethodCapture<T1, T2, T3, T4, T5, T6, T7>
      where TInstance : class
    {
        public void Bind(TInstance instance, Action<T1, T2, T3, T4, T5, T6, T7> target)
        {
            instance.When(o => target(Arg.Any<T1>(),
                Arg.Any<T2>(),
                Arg.Any<T3>(),
                Arg.Any<T4>(),
                Arg.Any<T5>(),
                Arg.Any<T6>(),
                Arg.Any<T7>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Func<T1, T2, T3, T4, T5, T6, T7, Task> target)
        {
            instance.When(o => target(Arg.Any<T1>(),
                Arg.Any<T2>(),
                Arg.Any<T3>(),
                Arg.Any<T4>(),
                Arg.Any<T5>(),
                Arg.Any<T6>(),
                Arg.Any<T7>()))
             .Do(NotifyTargetInvoked);
        }
    }

    public class
      InstanceMethodCapture<TInstance, T1, T2, T3, T4, T5, T6, T7, T8> : MethodCapture<T1, T2, T3, T4, T5, T6, T7, T8>
      where TInstance : class
    {
        public void Bind(TInstance instance, Action<T1, T2, T3, T4, T5, T6, T7, T8> target)
        {
            instance.When(o => target(Arg.Any<T1>(),
                Arg.Any<T2>(),
                Arg.Any<T3>(),
                Arg.Any<T4>(),
                Arg.Any<T5>(),
                Arg.Any<T6>(),
                Arg.Any<T7>(),
                Arg.Any<T8>()))
             .Do(NotifyTargetInvoked);
        }

        public void Bind(TInstance instance, Func<T1, T2, T3, T4, T5, T6, T7, T8, Task> target)
        {
            instance.When(o => target(Arg.Any<T1>(),
                Arg.Any<T2>(),
                Arg.Any<T3>(),
                Arg.Any<T4>(),
                Arg.Any<T5>(),
                Arg.Any<T6>(),
                Arg.Any<T7>(),
                Arg.Any<T8>()))
             .Do(NotifyTargetInvoked);
        }
    }
}
