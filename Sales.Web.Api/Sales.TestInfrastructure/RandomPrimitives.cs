﻿using Bogus;
using Bogus.DataSets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.TestInfrastructure
{
    public static class RandomPrimitives
    {
        private static readonly Faker Faker = CreateFaker();

        public static string DataUriImage()
        {
            return Faker.Image.DataUri(100, 100);
        }

        public static DateTime Date()
        {
            var date = Faker.Date.Between(new DateTime(1800, 1, 1), DateTime.MaxValue.AddDays(-1));

            return new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second);
        }

        public static DateTime RandomDateBetween(DateTime minValue, DateTime maxValue)
        {
            return Faker.Date.Between(minValue, maxValue);
        }

        public static DateTime DateOfBirth()
        {
            return Faker.Person.DateOfBirth;
        }

        public static decimal RandomMoneyValue()
        {
            return Faker.Finance.Amount();
        }

        public static bool RandomBoolean()
        {
            return Faker.Random.Bool();
        }

        public static string RandomIpAddress()
        {
            return CreateFaker().Internet.Ip();
        }

        public static string RandomJson()
        {
            return CreateFaker().Random.Words().ToJson();
        }

        public static string RandomString()
        {
            return Faker.Random.Utf16String();
        }

        public static string RandomVersionNumber()
        {
            return Faker.Random.Int(0, 10) + "." +
                   Faker.Random.Int(0, 10) + "." +
                   Faker.Random.Int(0, 10) + "." +
                   Faker.Random.Int(0, 10);
        }

        public static string RandomWord()
        {
            return Faker.Random.Word();
        }

        public static string RandomCountry()
        {
            return Faker.Address.Country();
        }

        public static string RandomWords()
        {
            return Faker.Random.Words();
        }

        public static string PolicyTypeConfigMetaData()
        {
            return "{\r\n\"displayName\": \"Partner Protection Plan\",\r\n\"displayDesc\": \"For your partner\",\r\n\"allowUsePolicyHolderDetails\": false,\r\n\"cardConfig\": {\r\n\"expandedDescription\": \"    \",\r\n\"cardInfo\": \"    \",\r\n\"quoteDesc\": \"   \",\r\n\"benefit\": \"   \",\r\n\"waitingPeriod\": \"    \",\r\n\"showCoverAmount\": true,\r\n\"coverLabel\": \"Your cover amount is: \",\r\n\"premiumLabel\": \"Your monthly premium is: \",\r\n\"prorataLabel\": \"Your prorata fee is: \"\r\n},\r\n\"insureds\": {\r\n\"relationshipToOwner\": {\r\n\"FirstAssured\": [ \"Spouse\" ]\r\n},\r\n\"forceUsePolicyHolderDetailsOn\": false\r\n}\r\n}";
        }

        public static int RandomNumber(int min = 0, int max = int.MaxValue)
        {
            return Faker.Random.Number(min, max);
        }

        public static int RandomNumberGreaterThanZero()
        {
            return Faker.Random.Number(min: 1);
        }

        public static string Gender()
        {
            return Faker.PickRandom("M", "F", "O");
        }

        public static string YesNo()
        {
            return Faker.PickRandom("Y", "N");
        }

        public static string AlphaNumeric(int length)
        {
            return Faker.Random.AlphaNumeric(length);
        }

        public static string PremiumType()
        {
            return Faker.Random.ArrayElement(new[] { "Single", "Monthly", "Annual" });
        }

        public static string PaymentMethod()
        {
            return Faker.Random.ArrayElement(new[] { "Cash", "DirectDebit", "Finance", "BankCollect", "Dealership", "RCSCollect" });
        }

        public static string InsuredType()
        {
            return Faker.Random.ArrayElement(new[] { "FirstAssured", "SecondAssured", "Child", "Spouse" });
        }

        public static string InsuredTypeWithoutChild()
        {
            return Faker.Random.ArrayElement(new[] { "FirstAssured", "SecondAssured", "Spouse" });
        }
        public static string Method()
        {
            return Faker.Random.ArrayElement(new[] { "GET", "POST", "PUT", "DELETE" });
        }

        public static bool Boolean()
        {
            return Faker.Random.Bool();
        }

        public static string RandomName()
        {
            return Faker.Random.Word();
        }

        public static string RandomUsername()
        {
            return CreateFaker().Person.UserName;
        }

        public static string RandomUrl()
        {
            return Faker.Internet.Url();
        }

        public static string RandomFirstname()
        {
            return CreateFaker().Person.FirstName;
        }

        public static string RandomLastname()
        {
            return CreateFaker().Person.LastName;
        }

        public static string RandomTitle()
        {
            return Pick(new[] { "Unspec", "Mr.", "Mnr", "Ms.", "Mrs.", "Mev", "Miss", "Mej", "Doctor", "Reverend", "Professor", "Honourable", "Advocate", "Misters" });
        }

        public static Guid RandomGuid()
        {
            return Faker.Random.Guid();
        }

        public static string RandomEmail()
        {
            return CreateFaker().Person.Email;
        }

        public static string RandomContactNumber()
        {
            return Faker.Phone.PhoneNumber("##########");
        }

        public static string ImageName()
        {
            return Faker.System.FileName(Faker.PickRandom("png", "jpg", "jpeg"));
        }

        public static string SofariIdType()
        {
            return Faker.PickRandom("Unspecified", "IdentityBook", "SouthAfricanPassport", "ForeignPassport");
        }

        public static string PassportSofariIdType()
        {
            return Faker.PickRandom("SouthAfricanPassport", "ForeignPassport");
        }

        public static int DbId()
        {
            return Faker.Random.Int(1);
        }

        public static string DbTableName()
        {
            return $"A{Faker.Random.AlphaNumeric(50)}A";
        }

        public static T Pick<T>(IEnumerable<T> values)
        {
            return Faker.PickRandom(values);
        }

        public static IList<T> Make<T>(int count, Func<T> factory)
        {
            return Faker.Make(count, factory);
        }

        private static Faker CreateFaker()
        {
            return new Faker();
        }

        public static string RandomRelationshipToOwner()
        {
            return Faker.PickRandom("AccountHolder", "Child", "Domestic", "Grandchild", "Guardian", "Parent", "Relative", "Spouse");
        }

        public static string RandomZaIdNumber()
        {
            // TODO we might want to put in the effort to actually generate a decent fake here.
            return Faker.PickRandom("8209305126088", "4201015126088", "7206015136088");
        }

        public static string RandomZaIdNumberForPeopleYoungerThan65()
        {
            // TODO we might want to put in the effort to actually generate a decent fake here.
            return Faker.PickRandom("8209305126088", "7206015136088");
        }

        public static Address RandomAddress()
        {
            return CreateFaker().Address;
        }

        public static IList<string> RandomCellphoneMakes()
        {
            return new List<string> { "HUAWEI", "NOKIA", "SAMSUNG", "IPHONE", "ONEPLUS" };
        }

        public static T[] ShuffleArray<T>(T[] array)
        {
            Random rnd = new Random();
            return array.OrderBy(x => rnd.Next()).ToArray();
        }

        public static string RandomAlphanumeric(int i)
        {
            return Faker.Random.AlphaNumeric(i);
        }

        public static string RandomIdType()
        {
            return Faker.PickRandom("SouthAfricanIdNumber");
        }

        public static string RandomCellphoneNumber()
        {
            return Faker.Phone.PhoneNumber("##########");
        }

        public static string RandomEmploymentStatus()
        {
            return Faker.PickRandom("Employed(SA)", "Self Employed", "Pensioner", "Unemployed");
        }

        public static string RandomFilename()
        {
            return Faker.System.FileName("pdf");
        }

        public static string RandomArrayElement(Array array)
        {
            return Faker.Random.ArrayElement(array);
        }
    }
}
