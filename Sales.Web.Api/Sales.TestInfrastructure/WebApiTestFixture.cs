﻿using System;
using Flurl.Http;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;

namespace Sales.TestInfrastructure
{
    public class WebApiTestFixture : IDisposable
    {
        public IHost Host { get; set; }

        public IFlurlRequest Request()
        {
            return new FlurlClient(Host.GetTestServer().BaseAddress.AbsoluteUri)
                .Configure(settings => settings.HttpClientFactory = new TestHttpClientFactory(Host))
                .Request()
                .AllowAnyHttpStatus();
        }

        public void Dispose()
        {
            Host?.Dispose();
        }
    }
}
