﻿using FluentAssertions;
using FluentAssertions.Execution;
using FluentAssertions.Primitives;

namespace Sales.TestInfrastructure
{
    public class Captured<TValue>
    {
        private readonly List<TValue> _capturedValues = new List<TValue>();

        public bool HasValue { get; private set; }
        public DateTime Date { get; private set; } = DateTime.MinValue;
        public TValue Value { get; private set; }

        public TValue GetValue()
        {
            return HasValue ? Value : default;
        }

        public void SetValue(TValue value)
        {
            Value = value;

            if (!HasValue)
            {
                HasValue = true;
                Date = DateTime.UtcNow;
            }

            _capturedValues.Add(value);
        }

        public TValue[] GetCapturedValues()
        {
            return _capturedValues.ToArray();
        }

        public bool HasCapturedMatch(Func<TValue, bool> match)
        {
            return GetCapturedValues()
             .Any(match);
        }

        public void Reset()
        {
            HasValue = false;
            _capturedValues.Clear();
            Value = default;
            Date = DateTime.MinValue;
        }
    }

#pragma warning disable IDE1006
    // ReSharper disable once InconsistentNaming
    public static class __CapturedExtensions
    {
        public static CapturedAssertions<TValue> Should<TValue>(this Captured<TValue> instance)
        {
            return new CapturedAssertions<TValue>(instance);
        }
    }
#pragma warning restore IDE1006

    public class CapturedAssertions<TValue> : ReferenceTypeAssertions<Captured<TValue>, CapturedAssertions<TValue>>
    {
        public CapturedAssertions(Captured<TValue> instance)
        {
            Subject = instance;
        }

        protected override string Identifier => "captured";

        public AndConstraint<ObjectAssertions> BeCaptured(
          Func<TValue, bool> match,
          string because = "",
          params object[] becauseArgs)
        {
            Execute.Assertion
             .BecauseOf(because, becauseArgs)
             .Given(() => Subject.HasValue)
             .ForCondition(hasValue => hasValue)
             .FailWith("Expected value to have been captured")
             .Then
             .ForCondition(b => Subject.GetCapturedValues()
                                 .Any(match))
             .FailWith("Expected value should have at least one matching value captured");

            return new AndConstraint<ObjectAssertions>(Subject.GetCapturedValues()
             .FirstOrDefault(match)
             .Should());
        }

        public AndConstraint<ObjectAssertions> BeCaptured(string because = "", params object[] becauseArgs)
        {
            Execute.Assertion
             .BecauseOf(because, becauseArgs)
             .Given(() => Subject.HasValue)
             .ForCondition(hasValue => hasValue)
             .FailWith("Expected value to have been captured");

            return new AndConstraint<ObjectAssertions>(Subject.Value.Should());
        }

        public AndConstraint<CapturedAssertions<TValue>> NotBeCaptured(
          Func<TValue, bool> match,
          string because = "",
          params object[] becauseArgs)
        {
            Execute.Assertion
             .BecauseOf(because, becauseArgs)
             .Given(() => Subject.GetCapturedValues())
             .ForCondition(values => !values.Any(match))
             .FailWith("Expected value should have no matching value captured");

            return new AndConstraint<CapturedAssertions<TValue>>(this);
        }

        public AndConstraint<CapturedAssertions<TValue>> NotBeCaptured(string because = "", params object[] becauseArgs)
        {
            Execute.Assertion
             .BecauseOf(because, becauseArgs)
             .Given(() => Subject.HasValue)
             .ForCondition(hasValue => !hasValue)
             .FailWith("Expected value not to have been captured");

            return new AndConstraint<CapturedAssertions<TValue>>(this);
        }

        public AndConstraint<CapturedAssertions<TValue>> BeCapturedAfter(
          Captured<TValue> other,
          string because = "",
          params object[] becauseArgs)
        {
            Execute.Assertion
             .BecauseOf(because, becauseArgs)
             .ForCondition(other != null)
             .FailWith("You can't assert against a null capture")
             .Then
             .ForCondition(other.HasValue)
             .FailWith("You can't assert against the order of the other capture as it has never received a value")
             .Then
             .ForCondition(Subject.HasValue)
             .FailWith("You can't assert against the order of a capture that has never had a value")
             .Then
             .Given(() => Subject.Date)
             .ForCondition(date => date > other.Date)
             .FailWith("Expected capture to have occured after other capture");

            return new AndConstraint<CapturedAssertions<TValue>>(this);
        }

        public AndConstraint<CapturedAssertions<TValue>> BeCapturedBefore(
          Captured<TValue> other,
          string because = "",
          params object[] becauseArgs)
        {
            Execute.Assertion
             .BecauseOf(because, becauseArgs)
             .ForCondition(other != null)
             .FailWith("Other capture is null")
             .Then
             .ForCondition(other.HasValue)
             .FailWith("Other capture has never received a value")
             .Then
             .ForCondition(Subject.HasValue)
             .FailWith("Capture has never received a value")
             .Then
             .Given(() => Subject.Date)
             .ForCondition(date => date < other.Date)
             .FailWith("Expected capture to have occured before other capture");

            return new AndConstraint<CapturedAssertions<TValue>>(this);
        }
    }
}
