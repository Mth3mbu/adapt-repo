﻿using System.Reflection;

namespace Sales.TestInfrastructure.Reflaction
{
    public interface IMethodEnumeratorFilterCriteria : IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>
    {
        IMethodEnumeratorFilter Named(string name);
        IMethodEnumeratorFilter Generic();
        IMethodEnumeratorFilter AttributedWith<TAttributeType>(bool inherit = false) where TAttributeType : Attribute;
        IMethodEnumeratorFilter AttributedWith(Type attributeType, bool inherit = false);

        IMethodEnumeratorFilter Abstract();
        IMethodEnumeratorFilter Static();

        IMethodEnumeratorFilter SpecializationOfSignature<TDelegate>() where TDelegate : Delegate;
        IMethodEnumeratorFilter ExactMatchForSignature<TDelegate>() where TDelegate : Delegate;
        IMethodEnumeratorFilter CompatibleMatchForSignature<TDelegate>() where TDelegate : Delegate;
    }

    public interface IMethodEnumeratorFilter : IEnumerable<MethodInfo>
    {
        IMethodEnumeratorFilterCriteria AndIs { get; }
        IMethodEnumeratorFilterCriteria AndNot { get; }
        IMethodEnumeratorFilterCriteria OrIs { get; }
    }
}
