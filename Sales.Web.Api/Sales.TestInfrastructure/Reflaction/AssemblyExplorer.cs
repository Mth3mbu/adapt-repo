﻿using System.Reflection;

namespace Sales.TestInfrastructure.Reflaction
{
    public class AssemblyExplorer : ICloneable
    {
        private bool _reflectionOnly;
        private readonly HashSet<Assembly> _addedAssemblies;
        private Type[] _types;

        public IReadOnlyList<Type> Types => _types;

        private AssemblyExplorer(AssemblyExplorer original)
        {
            _addedAssemblies = new HashSet<Assembly>(original._addedAssemblies);
            _types = original._types;
            _reflectionOnly = original._reflectionOnly;
        }

        public AssemblyExplorer()
        {
            _types = new Type[0];
            _addedAssemblies = new HashSet<Assembly>();
        }

        public void Add<TTypeContainedInAssembly>()
        {
            Add(typeof(TTypeContainedInAssembly).Assembly);
        }

        public void AddRange(Assembly[] assemblies)
        {
            var allTypes = new List<Type>(_types);

            foreach (var assembly in assemblies)
            {
                if (_addedAssemblies.Add(assembly))
                {
                    allTypes.AddRange(assembly.ReflectionOnly ? assembly.GetExportedTypes() : assembly.GetTypes());

                    if (assembly.ReflectionOnly)
                        _reflectionOnly = true;
                }
            }

            _types = allTypes.ToArray();
        }

        public void Add(Assembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException(nameof(assembly));

            if (_addedAssemblies.Add(assembly))
            {
                Type[] types = assembly.ReflectionOnly ? assembly.GetExportedTypes() : assembly.GetTypes();
                Type[] nTypes = new Type[types.Length + _types.Length];

                Array.Copy(_types, nTypes, _types.Length);
                Array.Copy(types, 0, nTypes, _types.Length, types.Length);

                _types = nTypes;

                if (assembly.ReflectionOnly)
                    _reflectionOnly = true;
            }
        }

        public ITypeEnumeratorFilterCriteria FilterTypes()
        {
            return new TypeEnumeratorFilter(_types, _reflectionOnly);
        }

        public AssemblyExplorer Clone()
        {
            return new AssemblyExplorer(this);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
