﻿using System.Collections;
using System.ComponentModel;
using System.Reflection;

namespace Sales.TestInfrastructure.Reflaction
{
    internal sealed class PropertyEnumeratorFilter : IPropertyEnumeratorFilter, IPropertyEnumeratorFilterCriteria
    {
        private static bool IsReadable(PropertyInfo examineProperty, MemberAccessibility minimumAccessibility)
        {
            if (!examineProperty.CanRead) return false;
            return examineProperty.GetReadAccessibility() >= minimumAccessibility;
        }

        private static bool IsWritable(PropertyInfo examineProperty, MemberAccessibility minimumAccessibility)
        {
            if (!examineProperty.CanWrite) return false;
            return examineProperty.GetWriteAccessibility() >= minimumAccessibility;
        }

        private static bool IsIndexer(PropertyInfo examineProperty, Type[] parameterTypes)
        {
            var indexParameters = examineProperty.GetIndexParameters();

            if (parameterTypes == null || parameterTypes.Length == 0)
            {
                return indexParameters.Length > 0;
            }

            return indexParameters.Length == parameterTypes.Length
                   && indexParameters.Select(info => info.ParameterType)
                    .SequenceEqual(parameterTypes);
        }

        private static bool IsStatic(PropertyInfo examineProperty)
        {
            var examineType = examineProperty.PropertyType;
            return examineType.IsClass && examineType.IsAbstract && examineType.IsSealed;
        }

        private static bool IsValueTypes(PropertyInfo examineProperty)
        {
            var examineType = examineProperty.PropertyType;
            return examineType.IsValueType;
        }

        private static bool IsReferenceTypes(PropertyInfo examineProperty)
        {
            return !IsValueTypes(examineProperty);
        }

        private static bool DescendantsOf(Type examineType, Type ancestorType)
        {
            if (examineType.IsValueType) return false;
            if (examineType == ancestorType) return false;
            Type currentType = examineType;

            while ((currentType = currentType.BaseType) != null)
            {
                if (currentType == ancestorType)
                    return true;
            }

            return false;
        }

        private static bool ExactlyAsVisible(PropertyInfo examineProperty, MemberAccessibility accessibility)
        {
            return examineProperty.GetAccessibility() == accessibility;
        }

        private static bool GreaterOrEqualVisible(PropertyInfo examineProperty, MemberAccessibility accessibility)
        {
            return examineProperty.GetAccessibility() >= accessibility;
        }

        private static bool LessOrEqualVisible(PropertyInfo examineProperty, MemberAccessibility accessibility)
        {
            return examineProperty.GetAccessibility() <= accessibility;
        }

        private static bool MoreVisible(PropertyInfo examineProperty, MemberAccessibility accessibility)
        {
            return examineProperty.GetAccessibility() > accessibility;
        }

        private static bool LessVisible(PropertyInfo examineProperty, MemberAccessibility accessibility)
        {
            return examineProperty.GetAccessibility() < accessibility;
        }

        private static bool IsDefined(PropertyInfo examineType, Type attributeType, bool inherit)
        {
            IList<CustomAttributeData> customAttributes = CustomAttributeData.GetCustomAttributes(examineType);

            foreach (CustomAttributeData attribute in customAttributes)
            {
                if (attribute.AttributeType == attributeType)
                    return true;

                if (inherit && DescendantsOf(attributeType, attribute.AttributeType))
                    return true;
            }

            return false;
        }

        private readonly PropertyInfo[] _properties;
        private readonly bool _reflectionOnly;
        private FilterMode _mode;

        private List<KeyValuePair<bool, Predicate<PropertyInfo>>> _operations;

        public IPropertyEnumeratorFilterCriteria AndAre
        {
            get
            {
                _mode = FilterMode.AndTrue;
                return this;
            }
        }

        public IPropertyEnumeratorFilterCriteria AndNot
        {
            get
            {
                _mode = FilterMode.AndFalse;
                return this;
            }
        }

        public PropertyEnumeratorFilter(PropertyInfo[] properties, bool reflectionOnly)
        {
            _properties = properties;
            _reflectionOnly = reflectionOnly;
        }

        public IEnumerator<PropertyInfo> GetEnumerator()
        {
            return _operations == null
              ? Enumerable.AsEnumerable(_properties)
               .GetEnumerator()
              : _properties.Where(OnApplyFilter)
               .GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private bool OnApplyFilter(PropertyInfo property)
        {
            for (int i = 0; i < _operations.Count; i++)
            {
                KeyValuePair<bool, Predicate<PropertyInfo>> kvp = _operations[i];

                if (kvp.Value(property) != kvp.Key)
                    return false;
            }

            return true;
        }

        // ReSharper disable NotResolvedInText
        private void AddOperation(Predicate<PropertyInfo> operation)
        {
            if (_operations == null)
                _operations = new List<KeyValuePair<bool, Predicate<PropertyInfo>>>();

            var comparand = _mode switch
            {
                FilterMode.AndTrue => true,
                FilterMode.AndFalse => false,
                _ => throw new InvalidEnumArgumentException("_mode", (int)_mode, typeof(FilterMode))
            };

            _operations.Add(new KeyValuePair<bool, Predicate<PropertyInfo>>(comparand, operation));
        }
        // ReSharper restore NotResolvedInText

        private IPropertyEnumeratorFilter AttributedWith(Type attributeType, bool inherit)
        {
            Predicate<PropertyInfo> operation;

            if (_reflectionOnly)
                operation = examineType => IsDefined(examineType, attributeType, inherit);
            else
                operation = examineType => examineType.IsDefined(attributeType, inherit);

            AddOperation(operation);
            return this;
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.AttributedWith<TAttributeType>(bool inherit)
        {
            return AttributedWith(typeof(TAttributeType), inherit);
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.AttributedWith(Type attributeType, bool inherit)
        {
            return AttributedWith(attributeType, inherit);
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.AssignableFrom(Type castType)
        {
            AddOperation(examineProperty => examineProperty.PropertyType.IsAssignableFrom(castType));
            return this;
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.AssignableAs(Type castType)
        {
            AddOperation(examineProperty => castType.IsAssignableFrom(examineProperty.PropertyType));
            return this;
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.Static()
        {
            AddOperation(IsStatic);
            return this;
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.ValueTypes()
        {
            AddOperation(IsValueTypes);
            return this;
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.ReferenceTypes()
        {
            AddOperation(IsReferenceTypes);
            return this;
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.Readable(MemberAccessibility minimumAccessibility)
        {
            AddOperation(examineProperty => IsReadable(examineProperty, minimumAccessibility));
            return this;
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.Writable(MemberAccessibility minimumAccessibility)
        {
            AddOperation(examineProperty => IsWritable(examineProperty, minimumAccessibility));
            return this;
        }

        IPropertyEnumeratorFilter IPropertyEnumeratorFilterCriteria.Indexers(params Type[] parameterTypes)
        {
            AddOperation(examineProperty => IsIndexer(examineProperty, parameterTypes));
            return this;
        }

        IPropertyEnumeratorFilter IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>.PublicAccessibility()
        {
            AddOperation(examineProperty => ExactlyAsVisible(examineProperty, MemberAccessibility.Public));
            return this;
        }

        IPropertyEnumeratorFilter IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>.
          ProtectedInternalAccessibility()
        {
            AddOperation(examineProperty => ExactlyAsVisible(examineProperty, MemberAccessibility.ProtectedInternal));
            return this;
        }

        IPropertyEnumeratorFilter IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>.InternalAccessibility()
        {
            AddOperation(examineProperty => ExactlyAsVisible(examineProperty, MemberAccessibility.Internal));
            return this;
        }

        IPropertyEnumeratorFilter IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>.ProtectedAccessibility()
        {
            AddOperation(examineProperty => ExactlyAsVisible(examineProperty, MemberAccessibility.Protected));
            return this;
        }

        IPropertyEnumeratorFilter IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>.PrivateAccessibility()
        {
            AddOperation(examineProperty => ExactlyAsVisible(examineProperty, MemberAccessibility.Private));
            return this;
        }

        IPropertyEnumeratorFilter IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>.MoreAccessibleThan(
          MemberAccessibility accessibility)
        {
            AddOperation(examineProperty => MoreVisible(examineProperty, accessibility));
            return this;
        }

        IPropertyEnumeratorFilter IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>.LessAccessibleThan(
          MemberAccessibility accessibility)
        {
            AddOperation(examineProperty => LessVisible(examineProperty, accessibility));
            return this;
        }

        IPropertyEnumeratorFilter IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>.AtMostAsAccessibleAs(
          MemberAccessibility accessibility)
        {
            AddOperation(examineProperty => LessOrEqualVisible(examineProperty, accessibility));
            return this;
        }

        IPropertyEnumeratorFilter IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>.AtLeastAsAccessibleAs(
          MemberAccessibility accessibility)
        {
            AddOperation(examineProperty => GreaterOrEqualVisible(examineProperty, accessibility));
            return this;
        }

        private enum FilterMode
        {
            AndTrue,
            AndFalse
        }
    }
}
