﻿using System.Reflection;

namespace Sales.TestInfrastructure.Reflaction
{
    public static class MethodBaseExtensions
    {
        public static MemberAccessibility GetAccessibility(this MethodBase method)
        {
            MemberAccessibility accessibility = 0;

            if (method.IsPublic) accessibility |= MemberAccessibility.Public;
            if (method.IsPrivate) accessibility |= MemberAccessibility.Private;
            if (method.IsAssembly || method.IsFamilyAndAssembly) accessibility |= MemberAccessibility.Internal;
            if (method.IsFamilyOrAssembly) accessibility |= MemberAccessibility.Protected | MemberAccessibility.Internal;
            if (method.IsFamily) accessibility |= MemberAccessibility.Protected;

            return accessibility;
        }
    }

    public static class MethodInfoExtensions
    {
        public static T CreateDelegate<T>(this MethodInfo method) where T : class
        {
            return Delegate.CreateDelegate(typeof(T), method) as T;
        }
    }
}
