﻿using System.Reflection;

namespace Sales.TestInfrastructure.Reflaction
{
    public static class TypeExtensions
    {
        public static MemberAccessibility GetAccessibility(this Type type)
        {
            MemberAccessibility accessibility = 0;

            if (type.IsNested)
            {
                if (type.IsNestedPublic) accessibility |= MemberAccessibility.Public;
                if (type.IsNestedPrivate) accessibility |= MemberAccessibility.Private;
                if (type.IsNestedAssembly || type.IsNestedFamANDAssem) accessibility |= MemberAccessibility.Internal;

                if (type.IsNestedFamORAssem) accessibility |= MemberAccessibility.Protected | MemberAccessibility.Internal;
                if (type.IsNestedFamily) accessibility |= MemberAccessibility.Protected;
            }
            else
            {
                if (type.IsPublic) accessibility = MemberAccessibility.Public;
                else accessibility = MemberAccessibility.Internal;
            }

            return accessibility;
        }

        public static IPropertyEnumeratorFilterCriteria FilterProperties(this Type type)
        {
            return new PropertyEnumeratorFilter(
              type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public),
              type.Assembly.ReflectionOnly);
        }

        public static IMethodEnumeratorFilterCriteria FilterMethods(this Type type)
        {
            var methodInfos = type.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
            return new MethodEnumeratorFilter(
              methodInfos,
              type.Assembly.ReflectionOnly);
        }

        public static bool IsDescendantOf(this Type type, Type ancestorType)
        {
            Type currentType = type;

            while ((currentType = currentType.BaseType) != null)
            {
                if (currentType == ancestorType)
                    return true;
                if (currentType.IsGenericType
                    && !currentType.IsGenericTypeDefinition
                    && currentType.GetGenericTypeDefinition() == ancestorType)
                    return true;
            }

            return false;
        }

        public static Type AsConstructedGenericAncestor(this Type type, Type ancestorGenericTypeDefinition)
        {
            if (!ancestorGenericTypeDefinition.IsGenericType || !ancestorGenericTypeDefinition.IsGenericTypeDefinition)
                throw new ArgumentException("Must be a generic type definition", nameof(ancestorGenericTypeDefinition));

            Type currentType = type;
            Type ancestorType = ancestorGenericTypeDefinition;

            while ((currentType = currentType.BaseType) != null)
            {
                if (currentType == ancestorType)
                    return currentType;
                if (currentType.IsGenericType
                    && !currentType.IsGenericTypeDefinition
                    && currentType.GetGenericTypeDefinition() == ancestorType)
                    return currentType;
            }

            return null;
        }
    }
}
