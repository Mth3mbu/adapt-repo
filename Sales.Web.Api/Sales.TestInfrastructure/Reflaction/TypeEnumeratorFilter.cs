﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sales.TestInfrastructure.Reflaction
{
    internal sealed class TypeEnumeratorFilter : ITypeEnumeratorFilter, ITypeEnumeratorFilterCriteria
    {
        private static bool IsStatic(Type examineType)
        {
            return examineType.IsClass && examineType.IsAbstract && examineType.IsSealed;
        }

        private static bool IsAbstract(Type examineType)
        {
            return !examineType.IsValueType && examineType.IsAbstract && !examineType.IsSealed;
        }

        private static bool IsSealed(Type examineType)
        {
            return examineType.IsSealed && !examineType.IsAbstract;
        }

        private static bool IsNested(Type examineType, Type declaringType)
        {
            return examineType.IsNested && examineType.DeclaringType == declaringType;
        }

        private static bool IsNested(Type examineType)
        {
            return examineType.IsNested;
        }

        private static bool IsInterfaces(Type examineType)
        {
            return examineType.IsInterface;
        }

        private static bool IsEnumerations(Type examineType)
        {
            return examineType.IsEnum;
        }

        private static bool IsAttributes(Type examineType)
        {
            return examineType == typeof(Attribute) || DescendantsOf(examineType, typeof(Attribute));
        }

        private static bool IsStructures(Type examineType)
        {
            return examineType.IsValueType && !examineType.IsEnum;
        }

        private static bool IsClass(Type examineType)
        {
            return examineType.IsClass;
        }

        private static bool IsValueTypes(Type examineType)
        {
            return examineType.IsValueType;
        }

        private static bool IsReferenceTypes(Type examineType)
        {
            return !IsValueTypes(examineType);
        }

        private static bool ChildrenOf(Type examineType, Type parentType)
        {
            return examineType.BaseType == parentType;
        }

        private static bool DescendantsOf(Type examineType, Type ancestorType)
        {
            if (IsValueTypes(examineType)) return false;
            if (examineType == ancestorType) return false;
            Type currentType = examineType;

            while ((currentType = currentType.BaseType) != null)
            {
                if (currentType == ancestorType)
                    return true;
                if (currentType.IsGenericType
                    && !currentType.IsGenericTypeDefinition
                    && currentType.GetGenericTypeDefinition() == ancestorType)
                    return true;
            }

            return false;
        }

        private static bool AncestorsOf(Type examineType, Type descendantType)
        {
            return DescendantsOf(descendantType, examineType);
        }

        private static bool ConstructedWithNoParameters(Type examineType, MemberAccessibility accessibility)
        {
            ConstructorInfo[] constructors = accessibility == MemberAccessibility.Public
              ? examineType.GetConstructors()
              : examineType.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            return constructors.Where(constructor => MethodBaseExtensions.GetAccessibility(constructor) >= accessibility)
             .Any(constructor => constructor.GetParameters()
                                  .Length
                                 == 0);
        }

        private static bool ConstructedWithParameters(
          Type examineType,
          MemberAccessibility accessibility,
          Type[] parameterTypes)
        {
            ConstructorInfo[] constructors = accessibility == MemberAccessibility.Public
              ? examineType.GetConstructors()
              : examineType.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            return constructors.Where(constructor => constructor.GetAccessibility() >= accessibility)
             .Any(constructor =>
                ParametersAreCompatible(constructor.GetParameters(), parameterTypes));
        }

        private static bool ParametersAreCompatible(ParameterInfo[] parameters, Type[] desiredParameterTypes)
        {
            if (parameters.Length != desiredParameterTypes.Length) return false;

            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo currentParameter = parameters[i];
                Type desiredType = desiredParameterTypes[i];
                if (!currentParameter.ParameterType.IsAssignableFrom(desiredType)) return false;
            }

            return true;
        }

        private static bool ExactlyAsVisible(Type examineType, MemberAccessibility accessibility)
        {
            return examineType.GetAccessibility() == accessibility;
        }

        private static bool GreaterOrEqualVisible(Type examineType, MemberAccessibility accessibility)
        {
            return examineType.GetAccessibility() >= accessibility;
        }

        private static bool LessOrEqualVisible(Type examineType, MemberAccessibility accessibility)
        {
            return examineType.GetAccessibility() <= accessibility;
        }

        private static bool MoreVisible(Type examineType, MemberAccessibility accessibility)
        {
            return examineType.GetAccessibility() > accessibility;
        }

        private static bool LessVisible(Type examineType, MemberAccessibility accessibility)
        {
            return examineType.GetAccessibility() < accessibility;
        }

        private static bool IsDefined(Type examineType, Type attributeType, bool inherit)
        {
            if (IsValueTypes(examineType)) return false;
            IList<CustomAttributeData> customAttributes = CustomAttributeData.GetCustomAttributes(examineType);

            foreach (CustomAttributeData attribute in customAttributes)
            {
                if (attribute.AttributeType == attributeType)
                    return true;

                if (inherit && DescendantsOf(attributeType, attribute.AttributeType))
                    return true;
            }

            return false;
        }

        private readonly Type[] _types;
        private readonly bool _reflectionOnly;
        private FilterMode _mode;

        private List<KeyValuePair<bool, Predicate<Type>>> _operations;

        public ITypeEnumeratorFilterCriteria AndAre
        {
            get
            {
                _mode = FilterMode.AndTrue;
                return this;
            }
        }

        public ITypeEnumeratorFilterCriteria AndNot
        {
            get
            {
                _mode = FilterMode.AndFalse;
                return this;
            }
        }

        public TypeEnumeratorFilter(Type[] types, bool reflectionOnly)
        {
            _types = types;
            _reflectionOnly = reflectionOnly;
        }

        public IEnumerator<Type> GetEnumerator()
        {
            return _operations == null
              ? Enumerable.AsEnumerable(_types)
               .GetEnumerator()
              : _types.Where(OnApplyFilter)
               .GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private bool OnApplyFilter(Type type)
        {
            for (int i = 0; i < _operations.Count; i++)
            {
                var kvp = _operations[i];
                if (kvp.Value(type) != kvp.Key)
                    return false;
            }

            return true;
        }

        // ReSharper disable NotResolvedInText
        private void AddOperation(Predicate<Type> operation)
        {
            if (_operations == null)
                _operations = new List<KeyValuePair<bool, Predicate<Type>>>();


            var comparand = _mode switch
            {
                FilterMode.AndTrue => true,
                FilterMode.AndFalse => false,
                _ => throw new InvalidEnumArgumentException("_mode", (int)_mode, typeof(FilterMode))
            };

            _operations.Add(new KeyValuePair<bool, Predicate<Type>>(comparand, operation));
        }
        // ReSharper restore NotResolvedInText

        private ITypeEnumeratorFilter AttributedWith(Type attributeType, bool inherit)
        {
            Predicate<Type> operation;

            if (_reflectionOnly)
                operation = examineType => IsDefined(examineType, attributeType, inherit);
            else
                operation = examineType => examineType.IsDefined(attributeType, inherit);

            AddOperation(operation);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.AttributedWith<TAttributeType>(bool inherit)
        {
            return AttributedWith(typeof(TAttributeType), inherit);
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.AttributedWith(Type attributeType, bool inherit)
        {
            return AttributedWith(attributeType, inherit);
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.AssignableFrom(Type castType)
        {
            AddOperation(examineType => examineType.IsAssignableFrom(castType));
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.AssignableAs(Type castType)
        {
            AddOperation(castType.IsAssignableFrom);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.DescendantsOf(Type ancestorType)
        {
            AddOperation(examineType => DescendantsOf(examineType, ancestorType));
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.AncestorsOf(Type descendantType)
        {
            AddOperation(examineType => AncestorsOf(examineType, descendantType));
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.ChildrenOf(Type parentType)
        {
            AddOperation(examineType => ChildrenOf(examineType, parentType));
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Static()
        {
            AddOperation(IsStatic);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Abstract()
        {
            AddOperation(IsAbstract);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Sealed()
        {
            AddOperation(IsSealed);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Nested()
        {
            AddOperation(IsNested);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Nested(Type declaringType)
        {
            AddOperation(examineType => IsNested(examineType, declaringType));
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Interfaces()
        {
            AddOperation(IsInterfaces);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Enumerations()
        {
            AddOperation(IsEnumerations);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Attributes()
        {
            AddOperation(IsAttributes);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Structures()
        {
            AddOperation(IsStructures);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.Classes()
        {
            AddOperation(IsClass);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.ValueTypes()
        {
            AddOperation(IsValueTypes);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.ReferenceTypes()
        {
            AddOperation(IsReferenceTypes);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.ConstructedWithNoParameters(MemberAccessibility accessibility)
        {
            AddOperation(examineType => ConstructedWithNoParameters(examineType, accessibility));
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.ConstructedWithParameters(
          MemberAccessibility accessibility,
          params Type[] parameterTypes)
        {
            AddOperation(examineType => ConstructedWithParameters(examineType, accessibility, parameterTypes));
            return this;
        }

        ITypeEnumeratorFilter IMemberEnumeratorFilterCriteria<ITypeEnumeratorFilter>.PublicAccessibility()
        {
            AddOperation(examineType => ExactlyAsVisible(examineType, MemberAccessibility.Public));
            return this;
        }

        ITypeEnumeratorFilter IMemberEnumeratorFilterCriteria<ITypeEnumeratorFilter>.ProtectedInternalAccessibility()
        {
            AddOperation(examineType => ExactlyAsVisible(examineType, MemberAccessibility.ProtectedInternal));
            return this;
        }

        ITypeEnumeratorFilter IMemberEnumeratorFilterCriteria<ITypeEnumeratorFilter>.InternalAccessibility()
        {
            AddOperation(examineType => ExactlyAsVisible(examineType, MemberAccessibility.Internal));
            return this;
        }

        ITypeEnumeratorFilter IMemberEnumeratorFilterCriteria<ITypeEnumeratorFilter>.ProtectedAccessibility()
        {
            AddOperation(examineType => ExactlyAsVisible(examineType, MemberAccessibility.Protected));
            return this;
        }

        ITypeEnumeratorFilter IMemberEnumeratorFilterCriteria<ITypeEnumeratorFilter>.PrivateAccessibility()
        {
            AddOperation(examineType => ExactlyAsVisible(examineType, MemberAccessibility.Private));
            return this;
        }

        ITypeEnumeratorFilter IMemberEnumeratorFilterCriteria<ITypeEnumeratorFilter>.MoreAccessibleThan(
          MemberAccessibility accessibility)
        {
            AddOperation(examineType => MoreVisible(examineType, accessibility));
            return this;
        }

        ITypeEnumeratorFilter IMemberEnumeratorFilterCriteria<ITypeEnumeratorFilter>.LessAccessibleThan(
          MemberAccessibility accessibility)
        {
            AddOperation(examineType => LessVisible(examineType, accessibility));
            return this;
        }

        ITypeEnumeratorFilter IMemberEnumeratorFilterCriteria<ITypeEnumeratorFilter>.AtMostAsAccessibleAs(
          MemberAccessibility accessibility)
        {
            AddOperation(examineType => LessOrEqualVisible(examineType, accessibility));
            return this;
        }

        ITypeEnumeratorFilter IMemberEnumeratorFilterCriteria<ITypeEnumeratorFilter>.AtLeastAsAccessibleAs(
          MemberAccessibility accessibility)
        {
            AddOperation(examineType => GreaterOrEqualVisible(examineType, accessibility));
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.GenericParametersPresent()
        {
            AddOperation(examineType => examineType.ContainsGenericParameters);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.GenericTypeDefinitions()
        {
            AddOperation(examineType => examineType.IsGenericTypeDefinition);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.GenericTypes()
        {
            AddOperation(examineType => examineType.IsGenericType);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.ConstructedGenericTypes(Type genericTypeDefinition)
        {
            AddOperation(examineType => examineType.IsConstructedGenericType
                                        && examineType.GetGenericTypeDefinition() == genericTypeDefinition);
            return this;
        }

        ITypeEnumeratorFilter ITypeEnumeratorFilterCriteria.ConstructedGenericTypes()
        {
            AddOperation(examineType => examineType.IsConstructedGenericType);
            return this;
        }

        private enum FilterMode
        {
            AndTrue,
            AndFalse
        }
    }
}
