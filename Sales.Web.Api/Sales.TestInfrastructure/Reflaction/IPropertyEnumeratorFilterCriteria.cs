﻿using System.Reflection;

namespace Sales.TestInfrastructure.Reflaction
{
    public interface IPropertyEnumeratorFilterCriteria : IMemberEnumeratorFilterCriteria<IPropertyEnumeratorFilter>
    {
        IPropertyEnumeratorFilter AttributedWith<TAttributeType>(bool inherit = false) where TAttributeType : Attribute;
        IPropertyEnumeratorFilter AttributedWith(Type attributeType, bool inherit = false);

        IPropertyEnumeratorFilter Readable(MemberAccessibility minimumAccessibility = MemberAccessibility.Private);
        IPropertyEnumeratorFilter Writable(MemberAccessibility minimumAccessibility = MemberAccessibility.Private);

        IPropertyEnumeratorFilter ValueTypes();
        IPropertyEnumeratorFilter ReferenceTypes();

        IPropertyEnumeratorFilter Static();

        IPropertyEnumeratorFilter Indexers(params Type[] parameterTypes);

        IPropertyEnumeratorFilter AssignableFrom(Type castType);
        IPropertyEnumeratorFilter AssignableAs(Type castType);
    }

    public interface IPropertyEnumeratorFilter : IEnumerable<PropertyInfo>
    {
        IPropertyEnumeratorFilterCriteria AndAre { get; }
        IPropertyEnumeratorFilterCriteria AndNot { get; }
    }
}
