﻿using System.Collections;
using System.ComponentModel;
using System.Reflection;

namespace Sales.TestInfrastructure.Reflaction
{
    internal sealed class MethodEnumeratorFilter : IMethodEnumeratorFilter, IMethodEnumeratorFilterCriteria
    {
        private static bool IsSignatureMatch(MethodInfo examineMethod, Type delegateType, bool exactMatch, bool specialization)
        {
            Func<Type, Type, bool> predicate;

            if (exactMatch)
            {
                predicate = (examineSignatureType, delegateSignatureType) => examineSignatureType == delegateSignatureType;
            }
            else if (!specialization)
            {
                predicate = (examineSignatureType, delegateSignatureType) => examineSignatureType.IsAssignableFrom(delegateSignatureType);
            }
            else
            {
                predicate = (examineSignatureType, delegateSignatureType) => delegateSignatureType.IsAssignableFrom(examineSignatureType);
            }

            // out parameter => info.ParameterType.IsByRef && info.IsOut
            // ref parameter => info.ParameterType.IsByRef && !info.IsOut
            var delegateMethod = (MethodInfo)delegateType.GetMember("Invoke")[0];

            var examineSignatureParameters = examineMethod.GetParameters();
            var examineSignatureReturnType = examineMethod.ReturnType;

            var delegateTypeParameters = delegateMethod.GetParameters();
            var delegateReturnType = delegateMethod.ReturnType;

            if (!predicate(examineSignatureReturnType, delegateReturnType))
                return false;

            if (examineSignatureParameters.Length != delegateTypeParameters.Length)
                return false;

            for (int i = 0; i < examineSignatureParameters.Length; i++)
            {
                var examineSignatureParameter = examineSignatureParameters[i];
                var delegateTypeParameter = delegateTypeParameters[i];

                if (examineSignatureParameter.IsOut != delegateTypeParameter.IsOut
                    || examineSignatureParameter.ParameterType.IsByRef != delegateTypeParameter.ParameterType.IsByRef)
                    return false;

                if (!predicate(examineSignatureParameter.ParameterType, delegateTypeParameter.ParameterType))
                    return false;
            }

            return true;
        }

        private static bool IsStatic(MethodInfo examineMethod)
        {
            return examineMethod.IsStatic;
        }

        private static bool IsAbstract(MethodInfo examineMethod)
        {
            return examineMethod.IsAbstract;
        }

        private static bool DescendantsOf(Type examineType, Type ancestorType)
        {
            if (examineType.IsValueType) return false;
            if (examineType == ancestorType) return false;
            Type currentType = examineType;

            while ((currentType = currentType.BaseType) != null)
            {
                if (currentType == ancestorType)
                    return true;
            }

            return false;
        }

        private static bool Named(MethodInfo examineMethod, string name)
        {
            return examineMethod.Name == name;
        }

        private static bool Generic(MethodInfo examineMethod)
        {
            return examineMethod.IsGenericMethodDefinition;
        }

        private static bool ExactlyAsVisible(MethodInfo examineMethod, MemberAccessibility accessibility)
        {
            return examineMethod.GetAccessibility() == accessibility;
        }

        private static bool GreaterOrEqualVisible(MethodInfo examineMethod, MemberAccessibility accessibility)
        {
            return examineMethod.GetAccessibility() >= accessibility;
        }

        private static bool LessOrEqualVisible(MethodInfo examineMethod, MemberAccessibility accessibility)
        {
            return examineMethod.GetAccessibility() <= accessibility;
        }

        private static bool MoreVisible(MethodInfo examineMethod, MemberAccessibility accessibility)
        {
            return examineMethod.GetAccessibility() > accessibility;
        }

        private static bool LessVisible(MethodInfo examineMethod, MemberAccessibility accessibility)
        {
            return examineMethod.GetAccessibility() < accessibility;
        }

        private static bool IsDefined(MethodInfo examineType, Type attributeType, bool inherit)
        {
            IList<CustomAttributeData> customAttributes = CustomAttributeData.GetCustomAttributes(examineType);

            foreach (CustomAttributeData attribute in customAttributes)
            {
                if (attribute.AttributeType == attributeType)
                    return true;

                if (inherit && DescendantsOf(attributeType, attribute.AttributeType))
                    return true;
            }

            return false;
        }

        private readonly MethodInfo[] _methods;
        private readonly bool _reflectionOnly;
        private FilterMode _mode;

        private List<KeyValuePair<bool, Predicate<MethodInfo>>> _operations;

        public IMethodEnumeratorFilterCriteria AndIs
        {
            get
            {
                _mode = FilterMode.AndTrue;
                return this;
            }
        }

        public IMethodEnumeratorFilterCriteria AndNot
        {
            get
            {
                _mode = FilterMode.AndFalse;
                return this;
            }
        }

        public IMethodEnumeratorFilterCriteria OrIs
        {
            get
            {
                _mode = FilterMode.OrTrue;
                return this;
            }
        }

        public MethodEnumeratorFilter(MethodInfo[] methods, bool reflectionOnly)
        {
            _methods = methods;
            _reflectionOnly = reflectionOnly;
        }

        public IEnumerator<MethodInfo> GetEnumerator()
        {
            return _operations == null
              ? Enumerable.AsEnumerable(_methods)
               .GetEnumerator()
              : _methods.Where(OnApplyFilter)
               .GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private bool OnApplyFilter(MethodInfo property)
        {
            for (int i = 0; i < _operations.Count; i++)
            {
                KeyValuePair<bool, Predicate<MethodInfo>> kvp = _operations[i];

                if (kvp.Value(property) != kvp.Key)
                    return false;
            }

            return true;
        }

        // ReSharper disable NotResolvedInText
        private void AddOperation(Predicate<MethodInfo> operation)
        {
            if (_operations == null)
                _operations = new List<KeyValuePair<bool, Predicate<MethodInfo>>>();

            var comparand = _mode switch
            {
                FilterMode.AndTrue => true,
                FilterMode.OrTrue => true,
                FilterMode.AndFalse => false,
                _ => throw new InvalidEnumArgumentException("_mode", (int)_mode, typeof(FilterMode))
            };

            if (_mode == FilterMode.OrTrue && _operations.Count > 0)
            {
                var left = _operations[^1];
                var right = new KeyValuePair<bool, Predicate<MethodInfo>>(comparand, operation);
                Predicate<MethodInfo> combinedOperation = info => left.Value(info) == left.Key || right.Value(info) == right.Key;
                operation = combinedOperation;
                comparand = true;
                _operations[^1] = new KeyValuePair<bool, Predicate<MethodInfo>>(comparand, operation);
            }
            else _operations.Add(new KeyValuePair<bool, Predicate<MethodInfo>>(comparand, operation));
        }
        // ReSharper restore NotResolvedInText

        private IMethodEnumeratorFilter AttributedWith(Type attributeType, bool inherit)
        {
            Predicate<MethodInfo> operation;

            if (_reflectionOnly)
                operation = examineType => IsDefined(examineType, attributeType, inherit);
            else
                operation = examineType => examineType.IsDefined(attributeType, inherit);

            AddOperation(operation);
            return this;
        }

        IMethodEnumeratorFilter IMethodEnumeratorFilterCriteria.Named(string name)
        {
            AddOperation(info => Named(info, name));
            return this;
        }

        IMethodEnumeratorFilter IMethodEnumeratorFilterCriteria.Generic()
        {
            AddOperation(Generic);
            return this;
        }

        IMethodEnumeratorFilter IMethodEnumeratorFilterCriteria.AttributedWith<TAttributeType>(bool inherit)
        {
            return AttributedWith(typeof(TAttributeType), inherit);
        }

        IMethodEnumeratorFilter IMethodEnumeratorFilterCriteria.AttributedWith(Type attributeType, bool inherit)
        {
            return AttributedWith(attributeType, inherit);
        }

        IMethodEnumeratorFilter IMethodEnumeratorFilterCriteria.SpecializationOfSignature<TDelegate>()
        {
            AddOperation(info => IsSignatureMatch(info, typeof(TDelegate), false, true));
            return this;
        }

        IMethodEnumeratorFilter IMethodEnumeratorFilterCriteria.ExactMatchForSignature<TDelegate>()
        {
            AddOperation(info => IsSignatureMatch(info, typeof(TDelegate), true, false));
            return this;
        }

        IMethodEnumeratorFilter IMethodEnumeratorFilterCriteria.CompatibleMatchForSignature<TDelegate>()
        {
            AddOperation(info => IsSignatureMatch(info, typeof(TDelegate), false, false));
            return this;
        }

        IMethodEnumeratorFilter IMethodEnumeratorFilterCriteria.Static()
        {
            AddOperation(IsStatic);
            return this;
        }

        IMethodEnumeratorFilter IMethodEnumeratorFilterCriteria.Abstract()
        {
            AddOperation(IsAbstract);
            return this;
        }

        IMethodEnumeratorFilter IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>.PublicAccessibility()
        {
            AddOperation(examineMethod => ExactlyAsVisible(examineMethod, MemberAccessibility.Public));
            return this;
        }

        IMethodEnumeratorFilter IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>.
          ProtectedInternalAccessibility()
        {
            AddOperation(examineMethod => ExactlyAsVisible(examineMethod, MemberAccessibility.ProtectedInternal));
            return this;
        }

        IMethodEnumeratorFilter IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>.InternalAccessibility()
        {
            AddOperation(examineMethod => ExactlyAsVisible(examineMethod, MemberAccessibility.Internal));
            return this;
        }

        IMethodEnumeratorFilter IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>.ProtectedAccessibility()
        {
            AddOperation(examineMethod => ExactlyAsVisible(examineMethod, MemberAccessibility.Protected));
            return this;
        }

        IMethodEnumeratorFilter IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>.PrivateAccessibility()
        {
            AddOperation(examineMethod => ExactlyAsVisible(examineMethod, MemberAccessibility.Private));
            return this;
        }

        IMethodEnumeratorFilter IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>.MoreAccessibleThan(
          MemberAccessibility accessibility)
        {
            AddOperation(examineMethod => MoreVisible(examineMethod, accessibility));
            return this;
        }

        IMethodEnumeratorFilter IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>.LessAccessibleThan(
          MemberAccessibility accessibility)
        {
            AddOperation(examineMethod => LessVisible(examineMethod, accessibility));
            return this;
        }

        IMethodEnumeratorFilter IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>.AtMostAsAccessibleAs(
          MemberAccessibility accessibility)
        {
            AddOperation(examineMethod => LessOrEqualVisible(examineMethod, accessibility));
            return this;
        }

        IMethodEnumeratorFilter IMemberEnumeratorFilterCriteria<IMethodEnumeratorFilter>.AtLeastAsAccessibleAs(
          MemberAccessibility accessibility)
        {
            AddOperation(examineMethod => GreaterOrEqualVisible(examineMethod, accessibility));
            return this;
        }

        private enum FilterMode
        {
            AndTrue,
            AndFalse,
            OrTrue
        }
    }
}
