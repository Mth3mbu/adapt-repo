﻿using DeepEqual.Syntax;
using NSubstitute;

namespace Sales.TestInfrastructure
{
    public static class NSubstituteExtensions
    {
        public static T IsEquivalentToPassedArgument<T>(this T subject)
        {
            return Arg.Is<T>(o => o.IsDeepEqual(subject));
        }
    }
}
