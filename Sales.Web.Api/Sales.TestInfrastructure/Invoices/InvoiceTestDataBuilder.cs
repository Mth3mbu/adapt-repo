﻿using Sales.Models;

namespace Sales.TestInfrastructure.Invoices
{
    public class InvoiceTestDataBuilder:TestDataBuilder<InvoiceTestDataBuilder, Invoice>
    {
        public InvoiceTestDataBuilder WithId(Guid value)
        {
            return WithProp(p => p.Id = value);
        }

        public InvoiceTestDataBuilder WithUserId(Guid value)
        {
            return WithProp(p => p.UserId = value);
        }

        public InvoiceTestDataBuilder WithBalance(Decimal value)
        {
            return WithProp(p => p.Balance = value);
        }

        public override InvoiceTestDataBuilder WithRandomProps()
        {
            return WithId(RandomPrimitives.RandomGuid())
                .WithUserId(RandomPrimitives.RandomGuid())
                .WithBalance(RandomPrimitives.RandomMoneyValue());
        }
    }
}
