﻿using NSubstitute;
using Sales.Interfaces.Intergration.sql;

namespace Sales.TestInfrastructure.Invoices
{
    public class InvoiceGatewayTestDataBuilder : TestDataBuilder<InvoiceGatewayTestDataBuilder, IInvoiceGateway>
    {
        public InvoiceGatewayTestDataBuilder WithCaptureCreateCustomer(out Captured<Models.Invoice> captured)
        {
            var localCaptured = new Captured<Models.Invoice>();
            captured = localCaptured;

            return WithProp(p => p.CreateInvoce(Arg.Do<Models.Invoice>(invoce => localCaptured.SetValue(invoce))));
        }

        public InvoiceGatewayTestDataBuilder WithUpdateCustomer(out Captured<Models.Invoice> captured)
        {
            var localCaptured = new Captured<Models.Invoice>();
            captured = localCaptured;

            return WithProp(p => p.UpdateteInvoce(Arg.Do<Models.Invoice>(invoice => localCaptured.SetValue(invoice))));
        }
    }
}
