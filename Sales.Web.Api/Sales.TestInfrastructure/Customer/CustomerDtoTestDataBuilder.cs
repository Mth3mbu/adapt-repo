﻿using Sales.Models;

namespace Sales.TestInfrastructure.Customer
{
    public class CustomerDtoTestDataBuilder : TestDataBuilder<CustomerDtoTestDataBuilder, CustomerDto>
    {
        public CustomerDtoTestDataBuilder WithFirstName(string value)
        {
            return WithProp(p => p.FirstName = value);
        }

        public CustomerDtoTestDataBuilder WithLastName(string value)
        {
            return WithProp(p => p.LastName = value);
        }

        public CustomerDtoTestDataBuilder WithEmail(string value)
        {
            return WithProp(p => p.Email = value);
        }

        public CustomerDtoTestDataBuilder WithPhone(string value)
        {
            return WithProp(p => p.Phone = value);
        }

        public CustomerDtoTestDataBuilder WithCustomerId(Guid value)
        {
            return WithProp(p => p.Id = value);
        }

        public CustomerDtoTestDataBuilder WithCustomerTypeId(Guid value)
        {
            return WithProp(p => p.TypeId = value);
        }

        public CustomerDtoTestDataBuilder WithCustomerTypeId(Decimal value)
        {
            return WithProp(p => p.Balance = value);
        }

        public override CustomerDtoTestDataBuilder WithRandomProps()
        {
            return WithFirstName(RandomPrimitives.RandomFirstname())
                .WithLastName(RandomPrimitives.RandomLastname())
                .WithEmail(RandomPrimitives.RandomEmail())
                .WithPhone(RandomPrimitives.RandomCellphoneNumber())
                .WithCustomerTypeId(RandomPrimitives.RandomGuid())
                .WithCustomerId(RandomPrimitives.RandomGuid())
                .WithCustomerTypeId(RandomPrimitives.RandomMoneyValue());
        }
    }
}
