﻿using NSubstitute;
using Sales.Interfaces.Intergration.sql;
using Sales.Models;

namespace Sales.TestInfrastructure.Customer
{
    public class CustomerGatewayTestDataBuilder:TestDataBuilder<CustomerGatewayTestDataBuilder, ICustomerGateway>
    {
        public CustomerGatewayTestDataBuilder WithCaptureCreateCustomer(out Captured<Models.Customer> captured)
        {
            var localCaptured = new Captured<Models.Customer>();
            captured = localCaptured;

            return WithProp(p => p.CreateCustomer(Arg.Do<Models.Customer>(customer => localCaptured.SetValue(customer))));
        }

        public CustomerGatewayTestDataBuilder WithUpdateCustomer(out Captured<Models.Customer> captured)
        {
            var localCaptured = new Captured<Models.Customer>();
            captured = localCaptured;

            return WithProp(p => p.UpdateCustomer(Arg.Do<Models.Customer>(customer => localCaptured.SetValue(customer))));
        }

        public CustomerGatewayTestDataBuilder WithGetAllCustomers(List<CustomerDto> customers)
        {

            return WithProp(p => p.GetAllCustomers().Returns(customers));
        }

        public CustomerGatewayTestDataBuilder WithGetCustomerByPhone(string phone, Models.Customer customer)
        {
            return WithProp(p => p.GetCustomerByPhone(phone).Returns(customer));
        }

        public CustomerGatewayTestDataBuilder WithGetCustomerByEmail(string email, Models.Customer customer)
        {
            return WithProp(p => p.GetCustomerByEmail(email).Returns(customer));
        }

        public CustomerGatewayTestDataBuilder WithGetCustomerById(Guid customerId, Models.Customer customer)
        {
            return WithProp(p => p.GetCustomerById(customerId).Returns(customer));
        }

        public CustomerGatewayTestDataBuilder WithCaptureDeleteCustomer(Guid customerId, bool isDeleted)
        {
            return WithProp(m => m.DeleteCustomer(customerId).Returns(isDeleted));
        }
    }
}
