﻿using NSubstitute;
using Sales.Interfaces.Domain;
using Sales.Interfaces.Output;
using Sales.Models;

namespace Sales.TestInfrastructure.Customer
{
    public class CustomerUseCaseTestDataBuilder: TestDataBuilder<CustomerUseCaseTestDataBuilder,ICustomerUseCase>
    {
        public CustomerUseCaseTestDataBuilder WithGetAllCustomersSuccess(IEnumerable<CustomerDto> customers)
        {
            return WithProp(p => p.GetAllCustomers(Arg.Do<ISuccessOrErrorPresenter<List<CustomerDto>, ErrorDto>>(presenter => presenter.Success(customers.ToList()))));
        }
    }
}
