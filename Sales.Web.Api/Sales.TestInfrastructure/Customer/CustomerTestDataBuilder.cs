﻿
namespace Sales.TestInfrastructure.Customer
{
    public class CustomerTestDataBuilder : TestDataBuilder<CustomerTestDataBuilder, Models.Customer>
    {
        public CustomerTestDataBuilder WithFirstName(string value)
        {
            return WithProp(p => p.FirstName = value);
        }

        public CustomerTestDataBuilder WithLastName(string value)
        {
            return WithProp(p => p.LastName = value);
        }

        public CustomerTestDataBuilder WithEmail(string value)
        {
            return WithProp(p => p.Email = value);
        }

        public CustomerTestDataBuilder WithPhone(string value)
        {
            return WithProp(p => p.Phone = value);
        }

        public CustomerTestDataBuilder WithCustomerId(Guid value)
        {
            return WithProp(p => p.Id = value);
        }

        public CustomerTestDataBuilder WithCustomerTypeId(Guid value)
        {
            return WithProp(p => p.TypeId = value);
        }

        public override CustomerTestDataBuilder WithRandomProps()
        {
            return WithFirstName(RandomPrimitives.RandomFirstname())
                .WithLastName(RandomPrimitives.RandomLastname())
                .WithEmail(RandomPrimitives.RandomEmail())
                .WithPhone(RandomPrimitives.RandomCellphoneNumber())
                .WithCustomerId(RandomPrimitives.RandomGuid())
                .WithCustomerTypeId(RandomPrimitives.RandomGuid());
        }
    }
}
