﻿using Microsoft.Extensions.DependencyInjection;

namespace Sales.TestInfrastructure
{
    public static class AspNetIocExtensions
    {
        public static void SwapScoped<TService, TImplementation>(this IServiceCollection services)
            where TImplementation : class, TService
        {
            if (services.Any(x => x.ServiceType == typeof(TService) && x.Lifetime == ServiceLifetime.Scoped))
            {
                var serviceDescriptors = services.Where(x => x.ServiceType == typeof(TService) && x.Lifetime == ServiceLifetime.Transient).ToList();
                foreach (var serviceDescriptor in serviceDescriptors)
                {
                    services.Remove(serviceDescriptor);
                }

                services.AddTransient(typeof(TService), typeof(TImplementation));
            }
        }

        public static void SwapScoped<TService>(this IServiceCollection services, TService instance)
        {
            if (services.Any(x => x.ServiceType == typeof(TService) && x.Lifetime == ServiceLifetime.Scoped))
            {
                var serviceDescriptors = services.Where(x => x.ServiceType == typeof(TService) && x.Lifetime == ServiceLifetime.Transient).ToList();
                foreach (var serviceDescriptor in serviceDescriptors)
                {
                    services.Remove(serviceDescriptor);
                }

                services.AddTransient(typeof(TService), provider => instance);
            }
        }

        public static void SwapTransient<TService>(this IServiceCollection services, TService instance)
        {
            if (services.Any(x => x.ServiceType == typeof(TService) && x.Lifetime == ServiceLifetime.Transient))
            {
                var serviceDescriptors = services.Where(x => x.ServiceType == typeof(TService) && x.Lifetime == ServiceLifetime.Transient).ToList();
                foreach (var serviceDescriptor in serviceDescriptors)
                {
                    services.Remove(serviceDescriptor);
                }

                services.AddTransient(typeof(TService), provider => instance);
            }
        }
    }
}

