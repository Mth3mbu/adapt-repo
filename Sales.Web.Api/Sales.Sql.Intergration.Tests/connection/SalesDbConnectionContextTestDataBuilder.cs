﻿using NSubstitute;
using Sales.Interfaces.Connection;
using Sales.TestInfrastructure;
using System.Data;

namespace Sales.Sql.Intergration.Tests.connection
{
    public class SalesDbConnectionContextTestDataBuilder: TestDataBuilder<SalesDbConnectionContextTestDataBuilder, ISalesDbConnectionContext>
    {
        public SalesDbConnectionContextTestDataBuilder WithConnection(IDbConnection connection)
        {
            return WithProp(o => o.GetConnection().Returns(connection));
        }

        public SalesDbConnectionContextTestDataBuilder WithTransaction(IDbTransaction transaction)
        {
            return WithProp(o => o.GetTransaction().Returns(transaction));
        }

        public SalesDbConnectionContextTestDataBuilder CapturingRollbacks(out Captured<bool> captured)
        {
            var localCaptured = new Captured<bool>();
            captured = localCaptured;

            return WithProp(o => o.When(context => context.Rollback()).Do(info => localCaptured.SetValue(true)));
        }
    }
}
