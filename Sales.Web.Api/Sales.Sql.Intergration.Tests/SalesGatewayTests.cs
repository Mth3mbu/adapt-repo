﻿using Dapper;
using FluentAssertions;
using NUnit.Framework;
using Sales.Interfaces.Intergration.sql;
using Sales.Intergration.sql;
using Sales.Models;
using Sales.Sql.Intergration.Tests.connection;
using Sales.Sql.Intergration.Tests.Infrastructure;
using Sales.TestInfrastructure;
using Sales.TestInfrastructure.Customer;

namespace Sales.Sql.Intergration.Tests
{
    [TestFixture(Category = TestCategories.Integration)]
    public class SalesGatewayTests
    {
        public static ITestDbConnectionContextFactory TestDbConnectionContextFactory;

        [OneTimeSetUp]
        public void Init()
        {
            var _setup = new SetupLocalDbForTesting(Guid.NewGuid().ToString(), "TestDb");
            TestDbConnectionContextFactory = _setup.TestDbConnectionContextFactory;
        }

        [Test]
        public async Task GivenCustomer_WhenAdding_ShouldSaveToDb()
        {
            // Arrange
            var customer = CustomerTestDataBuilder.Create()
                .WithRandomProps()
                .WithCustomerTypeId(new Guid("d01f284a-3f58-43e6-8d39-c03f9d4aac66"))
                .Build();
            using var context = TestDbConnectionContextFactory.Create();
            var sut = createCustomerGateway(context);
            // Act
            await ClearCustomerTable(context);
            await sut.CreateCustomer(customer);
            // Assert
            var expected = await sut.GetCustomerById(customer.Id);

            expected.FirstName.Should().Be(customer.FirstName);
            expected.LastName.Should().Be(customer.LastName);
            expected.Email.Should().Be(customer.Email);
            expected.Phone.Should().Be(customer.Phone);
            expected.Id.Should().Be(customer.Id);
        }

        [Test]
        public async Task GivenCustomer_WhenUpdating_ShouldSaveToDb()
        {
            // Arrange
            var customer = CustomerTestDataBuilder.Create()
                .WithRandomProps()
                .WithCustomerTypeId(new Guid("d01f284a-3f58-43e6-8d39-c03f9d4aac66"))
                .Build();

            var updateCustomer = CustomerTestDataBuilder.Create()
                .WithRandomProps()
                .WithCustomerTypeId(new Guid("d96f0a1f-126a-4a1a-9ad9-0ba619e03198"))
                .WithCustomerId(customer.Id)
                .Build();

            using var context = TestDbConnectionContextFactory.Create();
            var sut = createCustomerGateway(context);
            // Act
            await ClearCustomerTable(context);
            await sut.CreateCustomer(customer);
            await sut.UpdateCustomer(updateCustomer);
            // Assert
            var expected = await sut.GetCustomerById(customer.Id);

            expected.FirstName.Should().NotBe(customer.FirstName);
            expected.FirstName.Should().Be(updateCustomer.FirstName);
            expected.LastName.Should().NotBe(customer.LastName);
            expected.LastName.Should().Be(updateCustomer.LastName);
            expected.Email.Should().NotBe(customer.Email);
            expected.Email.Should().Be(updateCustomer.Email);
            expected.Phone.Should().NotBe(customer.Phone);
            expected.Phone.Should().Be(updateCustomer.Phone);
            expected.Id.Should().Be(customer.Id);
            expected.TypeId.Should().NotBe(customer.TypeId);
            expected.TypeId.Should().Be(updateCustomer.TypeId);
        }

        [Test]
        public async Task ShouldReturnAllCustomers()
        {
            // Arrange
            var costomerTypeId = new Guid("d01f284a-3f58-43e6-8d39-c03f9d4aac66");
            var customers = CustomerTestDataBuilder.BuildRandomCollection(count: 5);
            foreach (var customer in customers)
            {
                customer.TypeId = costomerTypeId;
            }
            using var context = TestDbConnectionContextFactory.Create();
            var sut = createCustomerGateway(context);
            // Act
            await ClearCustomerTable(context);
            await CreateCustomers(context, customers);
            // Assert
            var expected = await sut.GetAllCustomers();
            expected.Count().Should().Be(customers.Count());
        }
        private static ICustomerGateway createCustomerGateway(TestDbConnectionContext context)
        {
            var dbConnectionFactory = SalesDbConnectionContextTestDataBuilder.Create()
                .WithConnection(context.Connection)
                .WithTransaction(context.Transaction)
                .Build();

            return new CustomerGateway(dbConnectionFactory);
        }

        private static async Task ClearCustomerTable(TestDbConnectionContext context)
        {
            await context.Connection.ExecuteAsync("DELETE FROM [Customer];", transaction: context.Transaction);
        }

        private static async Task<List<CustomerDto>> GetCustomers(TestDbConnectionContext context)
        {
            const string sql = @"SELECT c.[Id]
                                  ,[TypeId]
	                              ,ct.[Type]
                                  ,[FirstName]
                                  ,[LastName]
                                  ,[Email]
                                  ,[Phone]
            FROM[Customer] c LEFT JOIN[CustomerType] ct ON c.[TypeId] = ct.[Id]";

            var persons = await context.Connection.QueryAsync<CustomerDto>(sql, null, context.Transaction);

            return persons.ToList();
        }

        private static async Task CreateCustomers(TestDbConnectionContext context, IEnumerable<Customer> customers)
        {
            var query = @"INSERT INTO [Customer]
           ([Id]
           ,[TypeId]
           ,[FirstName]
           ,[LastName]
           ,[Email]
           ,[Phone])
            VALUES (@Id,@TypeId, @FirstName,@LastName,@Email,@Phone)";
            await context.Connection.ExecuteAsync(query,
               customers, context.Transaction);
        }
    }
}
