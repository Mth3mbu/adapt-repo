﻿using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;
using Sales.Repositories.Migrations;

namespace Sales.Sql.Intergration.Tests.Infrastructure
{
    public class MigrationRunner
    {
        public void Migrate(string connectionString)
        {
            var serviceProvider = (IServiceProvider)new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddSqlServer()
                    .WithGlobalConnectionString(connectionString)
                    .ScanIn(typeof(IMigrationMarker).Assembly)
                    .For
                    .All()
                )
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .BuildServiceProvider(false);

            using var scope = serviceProvider.CreateScope();
            scope.ServiceProvider
                .GetRequiredService<IMigrationRunner>()
                .MigrateUp();
        }
    }
}
