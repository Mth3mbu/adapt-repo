﻿using System.Data.SqlClient;

namespace Sales.Sql.Intergration.Tests.Infrastructure
{
    public class LocalTestDbConnectionContextFactory : ITestDbConnectionContextFactory
    {
        public string ConnectionString { get; }

        public LocalTestDbConnectionContextFactory(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public TestDbConnectionContext Create()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var transaction = connection.BeginTransaction();

            return new TestDbConnectionContext
            {
                Connection = connection,
                Transaction = transaction
            };
        }
    }
}
