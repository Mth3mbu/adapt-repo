﻿using Sales.TestInfrastructure;
using System.Data;

namespace Sales.Sql.Intergration.Tests.Infrastructure
{
    public class TestDbConnectionContext : IDisposable, ITestDbConnectionContext
    {
        public IDbConnection Connection { get; set; }
        public IDbTransaction Transaction { get; set; }

        public void Dispose()
        {
            Transaction?.Dispose();
            Connection?.Dispose();
        }
    }
}
