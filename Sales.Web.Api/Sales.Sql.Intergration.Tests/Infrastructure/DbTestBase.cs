﻿using NUnit.Framework;

namespace Sales.Sql.Intergration.Tests.Infrastructure
{
    public class DbTestBase
    {
        protected ITestDbConnectionContextFactory TestDbConnectionContextFactory => SetupTestDb.TestDbConnectionContextFactory;

        [SetUp]
        public void SetUp()
        {
          
        }

        [TearDown]
        public void TearDown()
        {
        }
    }
}
