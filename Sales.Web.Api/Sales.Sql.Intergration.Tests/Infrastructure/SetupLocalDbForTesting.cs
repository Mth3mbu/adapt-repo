﻿using LocalDb;

namespace Sales.Sql.Intergration.Tests.Infrastructure
{
    public class SetupLocalDbForTesting : IDisposable
    {
        private readonly SqlInstance _testServer;

        public ITestDbConnectionContextFactory TestDbConnectionContextFactory { get; }

        public SetupLocalDbForTesting(string instanceName, string dbName)
        {
            _testServer = new SqlInstance(instanceName, connection => Task.CompletedTask);

            var testDb = _testServer.Build(dbName).GetAwaiter().GetResult();

            new MigrationRunner().Migrate(testDb.ConnectionString);

            TestDbConnectionContextFactory = new LocalTestDbConnectionContextFactory(testDb.ConnectionString);
        }

        public void Cleanup()
        {
            _testServer.Cleanup();
        }

        public void Dispose()
        {
            _testServer.Cleanup();
        }
    }
}
