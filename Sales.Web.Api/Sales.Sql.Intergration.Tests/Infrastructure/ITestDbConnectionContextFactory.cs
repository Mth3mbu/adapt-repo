﻿
namespace Sales.Sql.Intergration.Tests.Infrastructure
{
    public interface ITestDbConnectionContextFactory
    {
        string ConnectionString { get; }
        TestDbConnectionContext Create();
    }
}
