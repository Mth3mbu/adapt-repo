﻿using NUnit.Framework;

namespace Sales.Sql.Intergration.Tests.Infrastructure
{
    [SetUpFixture]
    public class SetupTestDb
    {
        private SetupLocalDbForTesting _setup = new SetupLocalDbForTesting(Guid.NewGuid().ToString(), "TestDb");
        public  static ITestDbConnectionContextFactory TestDbConnectionContextFactory;

        public SetupTestDb()
        {
            TestDbConnectionContextFactory = _setup.TestDbConnectionContextFactory;
        }



        [SetUp]
        public void SetUpFixture()
        {
            _setup = new SetupLocalDbForTesting(Guid.NewGuid().ToString(), "TestDb");
            TestDbConnectionContextFactory = _setup.TestDbConnectionContextFactory;
        }

        [OneTimeTearDown]
        public void TearDownFixture()
        {
            _setup?.Cleanup();
        }
    }
}
