﻿using Dapper;
using FluentAssertions;
using NUnit.Framework;
using Sales.Interfaces.Intergration.sql;
using Sales.Intergration.sql;
using Sales.Sql.Intergration.Tests.connection;
using Sales.Sql.Intergration.Tests.Infrastructure;
using Sales.TestInfrastructure;
using Sales.TestInfrastructure.Customer;
using Sales.TestInfrastructure.Invoices;
using System.Data.SqlClient;

namespace Sales.Sql.Intergration.Tests
{
    [TestFixture(Category = TestCategories.Integration)]
    public class InvoceGatewayTests
    {
        public static ITestDbConnectionContextFactory? TestDbConnectionContextFactory;

        [OneTimeSetUp]
        public void Init()
        {
            var _setup = new SetupLocalDbForTesting(Guid.NewGuid().ToString(), "TestDb");
            TestDbConnectionContextFactory = _setup.TestDbConnectionContextFactory;
        }

        [Test]
        public async Task GivenInvoiceWithInvalid_CustomerNumber_WhenAdding_ShouldThrowSqlException()
        {
            // Arrange
            var invoice = InvoiceTestDataBuilder.Create()
                .WithRandomProps()
                .Build();
            using var context = TestDbConnectionContextFactory.Create();
            var sut = CreateInvoiceGateway(context);
            // Act
            await ClearInvoiceTable(context);
            Func<Task> act = async () => await sut.CreateInvoce(invoice);
            // Assert
            await act.Should().ThrowAsync<SqlException>();
        }

        [Test]
        public async Task GivenInvoiceWithValid_CustomerNumber_WhenAdding_ShoudSaveToDb()
        {
            // Arrange
            var customer = CustomerTestDataBuilder.Create()
               .WithRandomProps()
               .WithCustomerTypeId(new Guid("d01f284a-3f58-43e6-8d39-c03f9d4aac66"))
               .Build();

            var invoice = InvoiceTestDataBuilder.Create()
                .WithRandomProps()
                .WithBalance(10M)
                .WithUserId(customer.Id)
                .Build();

            using var context = TestDbConnectionContextFactory.Create();
            var invoiceRepo = CreateInvoiceGateway(context);
            var customerRepo = CreateCustomerGateway(context);
            // Act
            await ClearInvoiceTable(context);
            await customerRepo.CreateCustomer(customer);
            await invoiceRepo.CreateInvoce(invoice);
            // Assert
            var expected = await GetInvoiceById(invoice.Id, context);

            expected.Should().NotBeNull();
            expected.Id.Should().Be(invoice.Id);
            expected.UserId.Should().Be(invoice.UserId);
            expected.Balance.Should().Be(invoice.Balance);
        }

        private static IInvoiceGateway CreateInvoiceGateway(TestDbConnectionContext context)
        {
            var dbConnectionFactory = SalesDbConnectionContextTestDataBuilder.Create()
           .WithConnection(context.Connection)
           .WithTransaction(context.Transaction)
           .Build();

            return new InvoiceGateway(dbConnectionFactory);
        }

        private static ICustomerGateway CreateCustomerGateway(TestDbConnectionContext context)
        {
            var dbConnectionFactory = SalesDbConnectionContextTestDataBuilder.Create()
                .WithConnection(context.Connection)
                .WithTransaction(context.Transaction)
                .Build();

            return new CustomerGateway(dbConnectionFactory);
        }


        private static async Task ClearInvoiceTable(TestDbConnectionContext context)
        {
            await context.Connection.ExecuteAsync("DELETE FROM [Invoice];", transaction: context.Transaction);
        }

        private async Task<Models.Invoice> GetInvoiceById(Guid id, TestDbConnectionContext context)
        {
            var query = @"SELECT * FROM [Invoice] WHERE [Id]=@id";

            return await context.Connection.QueryFirstOrDefaultAsync<Models.Invoice>(query, new { id }, context.Transaction);
        }
    }
}
