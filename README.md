## **WebApp**
1. Clone the Repo
2. Run **npm install**
3. Run **ng serve**  to run the app
4.  #**Run ng test** to run the tests

## **WebApi**
1. Go to **appsettings.Development**
2. Under **ConnectionStrings** replace **SalesDb** Value Which is **_Data Source=.;Initial Catalog=SalesDb;Integrated Security=True;** with own your connection string.
3. Build and run the App

**NB //The WebApi will automatically create all the Database tables and stored procs for you**
 
> NB Make sure on the enviroment.ts apiUrl matches the API url then
**You are good to go!**
